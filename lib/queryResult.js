

/**
 * Genera una istanza dell'oggetto `QueryResult`, valorizzato secondo gli argomenti della chiamata
 * 
 * @param {Boolean} success Se la query si è svolta senza errori.
 * @param {String} message Meesaggio da allegare in caso di insuccesso
 * @param {Any} data Varia da metodo a metodo. Generalmente l'oggetto *raw* restituito dalla libreria in uso.
 * @param {String} query Stringa utilizzata per la query al DB (se usata)
 * @param {Array} params Oggetto coi parametri della ricerca (Array o Dict)
 * @param {Array} insertedIds Oggetto con gli id inseriti (nel caso di INSERT)
 * @param {Number} affected Numero di record toccati dalla query 
 * @param {Number} changed Numero di record cambiati dalla query
 */
function QueryResult (
        success,
        message='',
        data=[],
        query='',
        params=[],
        insertedIds=[],
        affected=0,
        changed=0
        
) {
    return {
        success: success,
        message: message,
        data: data,
        query: query,
        params: params,
        insertedIds: insertedIds,
        affected: affected,
        changed: changed
        
    }

}; 

/**
 * Genera una istanza dell'oggetto `QueryResult`, con le proprietà `success` automaticamente `true` e `message` automaticamente `''`. 
 * 
 * Imposta gli altri valori secondo gli argomenti della chiamata alla funzione. 
 * 
 * @param {Any} data Varia da metodo a metodo. Generalmente l'oggetto *raw* restituito dalla libreria in uso.
 * @param {String} query Stringa utilizzata per la query al DB (se usata)
 * @param {Array} params Oggetto coi parametri della ricerca (Array o Dict)
 * @param {Array} insertedIds Oggetto con gli id inseriti (nel caso di INSERT)
 * @param {Number} affected Numero di record toccati dalla query 
 * @param {Number} changed Numero di record cambiati dalla query
 * 
 * @return {QueryResult} Istanza di QueryResult
 */

function QuerySuccess (
    data=[],
    query='',
    params=[],
    insertedIds=[],
    affected=0,
    changed=0
) {
    return QueryResult (
        true,
        '',
        data,
        query,
        params,
        insertedIds,
        affected,
        changed
        
        )
};

/**
 * Genera una istanza dell'oggetto `QueryResult`, con le proprietà `success` automaticamente `false`. 
 * 
 * Imposta gli altri valori secondo gli argomenti della chiamata alla funzione.
 * 
 * @param {String} message Meesaggio per spiegare le ragioni dell'insuccesso
 * @param {Any} data Varia da metodo a metodo. Generalmente l'oggetto *raw* restituito dalla libreria in uso.
 * @param {String} query Stringa utilizzata per la query al DB (se usata)
 * @param {Array} params Oggetto coi parametri della ricerca (Array o Dict)
 * @param {Array} insertedIds Oggetto con gli id inseriti (nel caso di INSERT)
 * @param {Number} affected Numero di record toccati dalla query 
 * @param {Number} changed Numero di record cambiati dalla query
 */

function QueryFail (
    message,
    data=[],
    query='',
    params=[],
    insertedIds=[],
    affected=0,
    changed=0
    
) {
    return QueryResult (
        false,
        message,
        data,
        query,
        params,
        insertedIds,
        affected,
        changed
        
        )
};

module.exports = {
    QuerySuccess:QuerySuccess,
    QueryFail:QueryFail,
    QueryResult: QueryResult
}