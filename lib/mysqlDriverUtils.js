"use strict"

const jsutils = require('leukos-tech-jsutils');
const MissingArgError = jsutils.custErrors.args.MissingArgError;
const WrongArgTypeError = jsutils.custErrors.args.WrongArgTypeError;

/**
 * Oggetto restituito dalle funzioni per l'ottenimento dei comandi mysql
 * @typedef QueryCmdObj Oggetto coi valori necessari alle query MySql
 * 
 * @param {String} query Stringa di query con i placeholder per l'escaping
 * @param {Array} parameters Array di valori da fornire come 2° argomento al metodo *pool.query()* per l'esecuzione della query stessa.
 */

"";


/** @constant {Number} MAX_VARCHAR_SIZE Valore soglia al di sopra del quale i type *string* vengono convertiti in *TEXT* e non più *VARCHAR* dalla funzione *getDataType(type, size)* */
const MAX_VARCHAR_SIZE = 20000;


/** @constant {Array} ALLOWED_DATA_TYPES Array contenente le stringhe valide per specificare i tipi di valore necessari alla definizione delle table di MySql  */
const ALLOWED_DATA_TYPES = [
    'string', 'char',
    'number', 'int', 'integer', 'intero',
    'float', 'double',
    'bool', 'boolean',
    'date', 'datetime', 'time', 'timestamp',
    'blob', 'image', 'audio', 'mediumblob', 'longblob' ,
]

/**
 * Restituisce una istanza dell'oggetto QueryCmdObj, con le proprietà *query* e *parameters*
 * @param {String} query 1° argomento per il metodo *pool.query()*: stringa con i placeholders per i valori in *parameters*.
 * @param {Array} parameters 2° argomento per il metodo *pool.query()*: valori da associare ai *placeholders*.
 * @returns {QueryCmdObj} Oggetto necessario alle query MySql.
 */
function _queryCmd (query, parameters) {return {query:query, parameters:parameters}};



/**
 * Restituisce l'oggetto QueryCmdObj necessario a recuperare l'ultimo id inserito presso il database.
 */
function RC_lastInserted_cmd() { return _queryCmd('LAST_INSERT_ID();', undefined); };

/**
 * Restituisce l'oggetto QueryCmdObj necessario ad ottenere l'elenco dei warnings del database.
 */
function WARNINGS_show_cmd() { return _queryCmd('SHOW WARNINGS;', undefined); };


/**
 * Restituisce la stringa di query e l'array di parametri necessari ad individuare, nella tavola _tableName_ i record le cui chiavi (contenute in _keys) corrispondano ai valori forniti con _values_
 *
 * @param {String} tableName Nome della table su cui operare
 * @param {Array} keys Array delle chiavi utilizzate come filtro. Se non fornito viene restituito il comando per ottenere tutti i record nella table.
 * @param {Array} values Array dei valori discriminanti per le chiavi fornite.
 * 
 * @returns {QueryCmdObj} Oggetto con le proprietà *query* e *parameters* da fornire come 1° e 2° argomento al metodo **mysql.pool.query(string, array)**.
 * 
 * @throws {RangeError} Seil numero di elementi forniti con *keys* è diverso dal numero di elementi forniti con *values*.
 */
function RC_find_cmd (tableName, keys=null, values=null) {
    
    let result;
    let header = `mysqlDriverUtils.getFindRecordCmd(${tableName}, ${keys}, ${values})`;

    /** Stringa di base */
    let base =`SELECT * FROM ${tableName}`; 
    let parameters = undefined;
        
    try {    

        /** Array chiavi fornito */
        if (keys && keys.length) {
            
            /** Solleva RangeError se la lunghezza degli array _keys_ e _values_ differisce. */
            if (keys.length != values.length) {throw new RangeError(`${header} - L'array fornito con 'keys' (${keys.length}) deve essere di lunghezza pari a quello fornito con 'values' (${values.length})`)};
            
            /** Inizializza Array Placeholders */ 
            let plc=[];
            
            /** Var _parameters_ trasformata in Array */
            parameters=[];
        
            /** Cicla gli indici dell'Array _keys_ */
            for (let i=0; i<keys.length; i++) {
                
                try {

                    /** Inserisce un placeholder per il valore nell'array dei placeholder */
                    plc.push(`${keys[i]} = ?`);
                    
                    /** Inserisce il valore all'indice corrente di _values_ nell'array parameters */
                    parameters.push(values[i]);
                    
                } catch(err) {

                    err.message = `${header} - Impossibile aggiungere valore corrente (${keys[i]}) agli array per la creazione del comando "${base}" - ${err.name}: ${err.message}`;
                    console.error(err.message);
                    throw (err);
                }
            };

            /** Valorizza il risultato componendo la stringa finale 
             * * placeholder uniti con ' AND ') 
             * * Aggiunta clausola 'WHERE'
             * */
            result = _queryCmd(
                `${base} WHERE ${plc.join(' AND ')};`,
                parameters
            );
    
        
        } else {
            /** Valorizza il risultato con la sola stringa base */
            result = _queryCmd(base+";", parameters);
        }
        
        /** Restituisce il risultato {query: String, parameters: Array|undefined} */
        return result;

    } catch (err) {
        err.message = `${header} - Errore generico - ${err.name}: "${err.message}"`;
        console.error(err.message);
        throw(err);
    }
};

/**
 * Restituisce il comando per restituire, per la chiave specificata, solo una occorrenza
 *
 * @param {String} tableName
 * @param {Array} keys Array di stringhe col nome delle voci da elencare
 */
function getlistRecordsValuesCmd(tableName, keys) {
    return `SELECT ${keys.join(', ')} FROM ${tableName}`; 
    

};

/**
 * Restituisce la stringa di query e i parametri necessari ad aggiornare la *table* fornita come 1° argomento secondo gli altri argomenti forniti.
 * 
 * @param {String} tableName Nome della table su cui operare
 * @param {Array} recordIds Array degli id dei record di cui aggiornare i valori
 * @param {Array} keys Array delle chiavi i cui valori vanno aggiornati nei record
 * @param {Array} values Array dei nuovi valori per le chiavi fornite (appaiati ad esse dell'indice nell'*array*)
 * 
 * @returns {QueryCmdObj} Oggetto con le proprietà `query` {string} e `parameters` {array} da fornire come argomenti al metodo `mysql.pool(query, parameters)`
 */
function RC_update_cmd (tableName, recordIds, keys, values, idKey="id") {

    let plc = [];
    let idPlc = [];
    let params = [];
    let base = `UPDATE ${tableName} SET`;
    for (let i in keys) {
        plc.push(`${keys[i]}=?`);
        params.push(values[i]);
    };

    for (let id of recordIds) {
        idPlc.push('?');
        params.push(id);
    }


    return [
        `${base} ${plc.join(', ')} WHERE ${idKey} IN (${idPlc.join(', ')})`,
        params
    ]
}

/**
 * Restituisce la stringa di query e i parametri da utilizzare per l'eliminazione degli id specificati.
 *
 * @param {String} tableName Nome della _table_ su cui operare
 * @param {Array} ids Array degli id dei record da eliminare
 * @param {string} [idKey='id'] Stringa identificante la chiave assegnata come PRIMARY (default="id");
 * @returns {QueryCmdObj} Oggetto con le proprietà `query` {string} e `parameters` {array} da fornire come argomenti al metodo `mysql.pool(query, parameters)`
 */
function RC_delete_cmd (tableName, ids, idKey='id') {
    
    let _idsPlaceholders = [];
    let _parameters = ids;
    let idplc = [];
    // Inserisce nell'array plc (placeholders) tanti '?' quanti sono gli id da eliminare
    for (let id of ids) idplc.push('?');

    return [
        `DELETE FROM ${tableName} WHERE ${idKey} IN (${idplc.join(',')})`,
        _parameters
    ]
    // return [
    //     `DELETE FROM ${tableName} WHERE ${idKey} = ?`,
    //     _parameters
    // ]
};


/**
 * Restituisce la stringa per l'inserimento di un nuovo campo nella *table* `tableName` in accordo agli altri argomenti forniti.
 * 
 * @param {String} tableName Nome della table su cui operare
 * @param {Array} keys Array delle chiavi della table
 * @param {Array} values Valori per il record da inserire
 * 
 * @returns {String} Stringa già compilata per la query, da fornire al metodo `mysql.pool.query()` come *unico argomento*.
 */
function RC_insert_cmd (tableName, keys, values) {
    let vals = '';
    
    for (let v of values) {
        if (vals) vals += ', ';
        vals += '?'
        
        // if (typeof v=='number') {
        //     vals += `${v}`
        // } else if (!v) {
        //     vals += 'NULL'
        // } else {
        //     vals += `'${v}'`
        // }
    }
    return `INSERT INTO ${tableName} (${keys.join(", ")}) VALUES (${vals});`

};

/**
 * Restituisce il comando per visualizzare la struttura di una determinata table
 *
 * @param {String} tableName Nome della table
 * 
 * @returns {QueryCmdObj} Oggetto con le proprietà `query` {string} e `parameters` {array} da fornire come argomenti al metodo `mysql.pool(query, parameters)`
 * 
 */
function TB_showSchema_cmd (tableName) {
    return _queryCmd(`DESCRIBE ${tableName}`);
};


/**
 * Restituisce il comando per richiedere l'elenco delle table definite
 *
 * @returns {String} Il comando
 */
function TBS_show_cmd () {return `SHOW TABLES;`}


/**
 * 
 * @param {String} tableName Nome della *table* da generare (se non esistente)
 * @param {TableSchema} schema Array di oggetti `TableField` per la definizione della strttura della table nel caso essa non esista.
 * 
 * @return {QueryCmdObj} Oggetto con le proprietà `query` {string} e `parameters` {array} da fornire come argomenti al metodo `mysql.pool(query, parameters)`
 */
const TB_create_cmd = function(tableName, schema) {
    let indexed = [];
    let primary = [];
    let keys = []
    let foreign = [];
    let finalCmd, finalPrim, finalInd, finalForeing;
    
    let cmd = `CREATE TABLE IF NOT EXISTS ${tableName}`;
    for (let schItem of schema) {
        // if (!schItem.keyName || !schItem.type) throw TypeError(`Every item in the schema array must contain 'keyName' (here "${schItem.keyName}") and 'type' (here "${schItem.type}") properies`);
        let _cmd = [];
        //Definizioni di base: Nome della chiave e tipo
        _cmd.push(schItem.keyName.toString().toLowerCase());
        _cmd.push(getDataType(schItem.type, schItem.size));
        
        // Statements opzionali
        if(schItem.isUnsigned) _cmd.push('UNSIGNED');
        if(schItem.notNull) _cmd .push('NOT NULL');
        
        if(schItem.autoIncrement) _cmd.push('AUTO_INCREMENT');
        if(schItem.isUnique) _cmd.push('UNIQUE');
        
        //Composizione della stringa e aggiunta all'array del comando
        keys.push(_cmd.join(' '));
        
        // Aggiunge la chiave all'array _primary_ (se necessario)
        if(schItem.isPrimary) primary.push(schItem.keyName);
        // Aggiunge la chiave all'array _indexed_ (se necessario)
        if(schItem.isIndex) indexed.push(schItem.keyName);
        
        // Aggiunge la stringa per i riferimenti incrociati
        if(schItem.isReference) {
            foreign.push(_getForeignKeysCmd(
                schItem.keyName,
                schItem.reference.tableName,
                schItem.reference.tableField
            ));
        };
        
    };
    finalPrim = (primary.length) ? `PRIMARY KEY (${primary.join(', ')})` : '';
    finalInd = (indexed.length) ? `INDEX (${indexed.join(', ')})` : '';
    finalForeing = (foreign.length) ? foreign.join(', '): '';

    

    finalCmd = `${cmd} ( ${keys.join(', ')}`;
    finalCmd += (finalPrim != '') ? `, ${finalPrim}` : '';
    finalCmd += (finalInd != '') ? `, ${finalInd}` : '';
    finalCmd += (finalForeing != '') ? `, ${finalForeing}` : '';
    finalCmd += ');';

    return finalCmd;

    
};

/**
 * Restituisce il troncone di stringa relativo alla referenziazione di una determinata chiave con un campo da una table esterna (*tool* per `cmd.TB_create()`).
 * 
 * @param {String} keyName Nome della chiave
 * @param {String} foreignTable Nome della *table* esterna
 * @param {String} foreignFieldName Nome del *campo* della *Table* esterna
 * 
 * @returns {String} La stringa risultante dai parametri forniti
 */
function _getForeignKeysCmd(keyName, foreignTable, foreignFieldName) {   
    if (!keyName || !foreignTable || !foreignFieldName) {
        throw new TypeError(`mysqlDriverUtils.getForeignKeysCmd(${keyName}, ${foreignTable}, ${foreignFieldName}): All arguments are mandatory`);
    }
    return `FOREIGN KEY (${keyName}) REFERENCES ${foreignTable} (${foreignFieldName})`;
}

/**
 * Restituisce *solamente la stringa per la query* necessaria ad ottenere tramite l'interfaccia *pool.query(string)* l'**elenco dei database presenti** (il valore di *parameters* sempre **undefined**).
 */
function DBS_show_cmd() {
    return _queryCmd(`SHOW DATABASES;`); 
    // return "SELECT table_schema FROM INFORMATION_SCHEMA.tables";
};

/** 
 * Restituisce **la sola stringa di query** per impostare il `dbName` fornito come argomento, come `database` in uso. 
 * 
 * @param {String} dbName Nome del `database` da selezionare.
 * 
 * @returns {String} La stringa di query per impostare il database.
 * */
const DB_use_cmd = function (dbName) {return `USE ${dbName}`;}



/**
 * Restituisce la stringa necessaria ad eseguire la query mysql per creare (se non esiste) il database *dbName*
 * @param {String} dbName Nome del database mysql da creare col comando
 * @returns {QueryCmdObj} Oggetto con le proprietà `query` {string} e `parameters` {array} da fornire come argomenti al metodo `mysql.pool(query, parameters)`
 */
function DB_create_cmd (dbName) { return _queryCmd(`CREATE DATABASE IF NOT EXISTS ${dbName};`); }

/** 
 * @deprecated Utilizzare `jsutils.validators.validatePoolParams()`.
 * 
 * Restituisce `true` se l'oggetto fornito come argomento rispetta il formato richiesto per inizializzare l'oggetto params
 * 
 * @param {Object} params Oggetto coi parametri da verificare che deve presentare le proprietà:
 * * database
 * * user
 * * password
 * @return {Boolean} _true_ se il formato è valido
 */
function validatePoolParams (params) {
    if (!params.database || !params.user || !params.password ) {
        return false
    };
    return true
    
};

/**
 * Verifica che le proprietà fondamentali dell'array di TableFields contenga le proprietà fondamentali
 * @param {Array} schema Schema da validare 
 * 
 * @returns {Boolean} `true` se l'oggetto ha passato la verifica (mai `false`: solleva errori)
 * 
 * @throws {MissingArgError} Se una delle proprietà necessarie manca (valido anche per quelle in `reference` se `isReference` è `true`).
 * 
 * @throws {WrongArgTypeError} Se il type di una delle proprietà non è quello atteso
 */
function validateTableSchema(schema) {
    
    // Proprietà attese come booleane
    let booleanProps = [
        "isUnsigned",
        "autoIncrement",
        "notNull",
        "isPrimary",
        "isIndex",
        "isUnique",
        "isReference"
    ];

    
    
    // Cicla l'array dei TableField
    for (let i in schema) {
        // console.log(schema[i])
        if (!schema[i].keyName) {
            throw new MissingArgError(`ITEM[${i}].keyName`);
            // console.error("KEYNAME NON ESISTE!!!!!!!!!!!!!!!!!!!!!!")
            
        };
        if (!schema[i].type) {
            throw new MissingArgError(`ITEM[${i}].type`);
        };

        if (typeof schema[i].keyName != "string") {
            throw new WrongArgTypeError(`ITEM[${i}].keyName`, schema[i].keyName, "string");
        };

        if (typeof schema[i].type != "string") {
            throw new WrongArgTypeError(`ITEM[${i}].type`, schema[i].type, "string");
        };

        // Verifica il type delle proprietà booleane
        for (let prop of booleanProps) {
            // Salta il type check per proprietà assenti
            if (!schema[i][prop]) {continue}
            if (typeof schema[i][prop] != 'boolean') {
                throw new WrongArgTypeError(`ITEM[${i}].${prop}`, schema[i].type, "boolean");
            };

            // La proprietà è 'isReference' ed è `true`
            if (prop=="isReference" && schema[i].isReference) {

                // Nell'oggetto `reference` manca la proprietà `tableName`
                if (!schema[i].reference.tableName) {
                    throw new MissingArgError(`ITEM[${i}].reference.tableName (\`isReference\` è \`true\`)`);
                };

                // Nell'oggetto `reference` la proprietà `tableName` non è una stringa
                if (typeof schema[i].reference.tableName != "string") {
                    throw new WrongArgTypeError(`ITEM[${i}].reference.tableName (\`isReference\` è \`true\`)`, schema[i].reference.tableName, "string");
                };

                // Nell'oggetto `reference` manca la proprietà `tableField`
                if (!schema[i].reference.tableField) {
                    throw new MissingArgError(`ITEM[${i}].reference.tableField (\`isReference\` è \`true\`)`);
                };

                if (typeof schema[i].reference.tableField != "string") {
                    throw new WrongArgTypeError(`ITEM[${i}].reference.tableField (\`isReference\` è \`true\`)`, schema[i].reference.tableField, "string");
                };
            }
        };

        // console.warn("VALIDAZIONE SCHEMA SUPERATA")
        

    };

    return true

}

/**
 * Restituisce la stringa che indica il corretto *datatype* per la creazione di una table nel database in base ai parametri forniti.
 * @param {String} type Stringa identificativa del datatype da impostare per un determinato campo. I valori ammessi sono elencati nella costante **ALLOWED_DATA_TYPES** (
    'string', 'char',
    'number', 'int', 'integer', 'intero',
    'float', 'double',
    'bool', 'boolean',
    'date', 'datetime', 'time', 'timestamp',
    'blob', 'image', 'audio', 'mediumblob', 'longblob')
 * @param {Number} size [OPZIONALE] Dimensione da assegnare al campo.
 * 
 * Regole per il parametro *size*:
 * 
 * * Viene **ignorato** per i *type* **"blob"**, **"image"**, **"audio"**, **"mediumblob"**, **"longblob"** (sempre restituito un **"LONGBLOB"**).
 * 
 * * Per il *type* **"string"** se il valore fornito supera il valore fissato in *MAX_VARCHAR_SIZE* viene restituito **"TEXT"**. Altrimenti **"VARCHAR(s)"** con *s=size*. Se *size* non viene fornito restituisce **"VARCHAR(255)"**
 * 
 * * Per il *type* **"char"** il **valore massimo possibile è 255**. Con valori superiori, uguali a 0, o el caso non sia fornito affatto, la funzione solleva *RangeError*.
 * 
 * * Per *type* **"number"**, **"int"**, **"integer"**, **"intero"**
 * 
 * > - con `size==undefined`* o `size==2` restituisce **"SMALLINT"**;
 * > - con `size==1` restituisce **"TINYINT"**;
 * > - con `size==3` restituisce **"MEDIUMINT"**;
 * > - con `size==4` restituisce **"INT"**;
 * > - con `size>=5`, `size<=8` restituisce **"BIGINT"**
 * 
 * * Viene **ignorato** per gli altri *type*s.
 * 
 * @returns {String} La stringa da inserire nel comando di query col corretto datatype *MySql*
 * 
 * @throws {TypeError} Se l'argomento `type` non contiene un valore corretto.
 */
const getDataType = function(type, size=undefined, ) {

    let header = `mysqlDriverUtils.getDataType(${type}, ${size})`;
    // Assicura che size sia un valore numerico positivo
    if (size != undefined) {size = Math.abs(Number(size))}
    
    // Elabora il valore di 'type' 
    // (portandolo prima a lowercase string)
    switch (type.toString().toLowerCase()) {
        case 'blob':
        case 'image':
        case 'audio':
        case 'mediumblob':
        case 'longblob':
            return "LONGBLOB"

        case 'string':
            if (size!=undefined) {
                if (size >= MAX_VARCHAR_SIZE) {return 'TEXT'}
                //if (size <= 20) {return `CHAR(${size})`}
                return `VARCHAR(${size})`

            }
            return 'VARCHAR(255)';
        case 'char':
            let errMsg = `${header} - Con type CHAR è necessario specificare un valore, per l'argomento size, compreso tra 1 e 255`;
            // size deve essere compreso tra 1 e 255 e sempre fornito con char
            if (!size || size>255) throw RangeError(errMsg);
            return `CHAR(${size})`
            
        case 'number':
        case 'int':
        case 'integer':
        case 'intero':
            if (size!=undefined){
                if (size>8 || size == 0) {throw TypeError(`${header} - Con type ${type} l'argomento \`size\` può essere undefined (RESTITUISCE "SMALLINT") o compreso tra 1 è 8`)};
                switch (size){
                    case 1: return 'TINYINT';
                    case 3: return 'MEDIUMINT';
                    case 4: return 'INT';
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                        return 'BIGINT';
                    
                    
                }
            }
            return 'SMALLINT'
        
        case 'bool':
        case 'boolean':
            return 'BOOLEAN';
        
        case 'date':
            return 'DATE'
        case 'datetime':
            return 'DATETIME';
        
        case 'time':
            return 'TIME';
        case 'timestamp':
            return 'TIMESTAMP';
        case 'float':
            return 'FLOAT';
        case 'double':
            return 'DOUBLE';
        default:
            throw TypeError(`'type' field has incorrect value: '${type.toString().toLowerCase()}'`);

    }
};


module.exports = {

    MAX_VARCHAR_SIZE: MAX_VARCHAR_SIZE,
    ALLOWED_DATA_TYPES: ALLOWED_DATA_TYPES,

    getDataType : getDataType,
    
    validateTableSchema : validateTableSchema,
    cmd: {
        DB_create:          DB_create_cmd,
        DBS_show:           DBS_show_cmd,
        DB_use:             DB_use_cmd,
        TBS_show:           TBS_show_cmd,
        TB_create:          TB_create_cmd,
        TB_showSchema:      TB_showSchema_cmd,
        RC_insert:          RC_insert_cmd,
        RC_delete:          RC_delete_cmd,
        RC_update:          RC_update_cmd,
        RC_find:            RC_find_cmd,
        RC_lastInserted:    RC_lastInserted_cmd,
        WRN_show:           WARNINGS_show_cmd
    }

};

