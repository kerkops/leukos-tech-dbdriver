"use strict";

const DbDriverInterface = require('./driverInterface');


/**
 * Classe astratta che eredita l'interfaccia di DbDriverInterface e aggiunge agli oggetti di tipo driver che la ereditano la funzionalità di logging e debug.
 *
 * @class DbDriver
 * @extends {DbDriverInterface}
 */
class DbDriver extends DbDriverInterface  {

    /**
     * Costruttore dell'Oggetto Astratto DbDriver.
     * Genera un oggetto con gli attributi relativi al logging valorizzati.
     * @param {Boolean} debug Se True attiva il logging delle informazioni addizionali tramite il logger fornito come "logger" o tramite la console se non fornito
     * @param {Object} logger Oggetto da utilizzare per il logging che deve presentare i metodi _.debug(msg)_, _warn(msg)_, _error(msg)_. Se non fornita la console verrà utilizzata. 
     */
    constructor(debug=false, logger=null) {

        // Inizializza i metodi dell'interfaccia
        super();

        /** @type {Boolean} Se True il logging viene attivato */
        this._debugOn = debug;
        
        /** @type {Object} Oggetto da utilizzare per il logging dei dati */
        this._logger = logger || console;

        this._log('debug', `DbDriver - DEBUG ATTIVO: ${debug}`)

    };

    /**
     * Funzione di convenienza per il log delle informazioni
     * @param {String} level Livello di logging desiderato (solleva un WARNING se il livello non compare tra i metodi del logger)
     * @param {String} message Messaggio da loggare
     */
    _log (level, message) {

        /**
         * * Il Debug è attivo
         * * Il Livello è 'error' o 'warn'
         */
        if (this._debugOn || ['error', 'warn'].includes(level.toString().toLowerCase())) {
            
            // la stringa 'level' contiene un nome valido di livello di logging        
            if (Object.keys(this._logger).includes(level)) {

                // Alias per il metodo relativo al livello da invocare
                let _logger = this._logger[level];

                // esegue il logging al livello richiesto.
                _logger(message)
            
            // La stringa 'level' non è valida
            } else {
                this._logger.warn(`!WARNING! Invalid 'level' attribute for current logger: ${level}`);
            }
        }
    };
    

};

module.exports = DbDriver;