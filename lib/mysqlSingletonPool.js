"use strict";

const mysql = require('mysql');
const nodeUtil = require('util');
// let l = Math.random().toString(36)

/** @constant {Map} _INSTANCES Oggetto di tipo `Map` che contiene le istanze della pool al DB MySql */
const _INSTANCES = new Map();
/**
 * Generatore di Singleton dell'oggetto `pool` (dal package `mysql`).
 * 
 * @static Contiene soltanto metodi ed attrivuti statici
 * 
 * Unico metodo è `mysqlSingletonPool.get(poolParams, debug, logger)`
 * 
 * 
 */
class mysqlSingletonPool {

    /**
     * Proprietà **STATICA** contenente la `Map` che detiene le istanze della `pool` già generate, indicizzate tramite i parametri con cui sono state generate.
     *
     * @readonly
     * @static `mysqlSingletonPool.instances`
     * @memberof mysqlSingletonPool
     */
    static get instances() {return _INSTANCES}


    /**
     * Metodo **STATICO** che restituisce un Singleton di `mysql.pool` all'invariare dei parametri forniti come argomento.
     * 
     * @param {any} params Oggetto dati contenente i parametri per la connessione
     * @param {boolean} debug Se `true` verranno loggate informazioni tramite il `logger` o tramite la `console`.
     * @param {any} _logger Istanza del logger da utilizzare al posto di `console` (deve contenere il metodo `error({String})`).
     * 
     * ---
     * 
     * @async *Promise(`err`, `poolSingleton`)
     * 
     * Si risolve nell'oggetto `pool` da utilizzare per interagire col DB MySql specificato in `params.database`.
     * 
     * ---
     * 
     * **COMPORTAMENTO**
     * * Restituisce sempre un **SINGLETON** se i parametri forniti sono sempre i medesimi [TC MSP01].
     * * I Singleton restituiti sono **INTERCONNESSI** (ovviamente): quando viene eseguito il metodo `.end()` **SU UNA QUALSIASI DELLE ISTANZE, *TUTTE* VENGONO CHIUSE E DIVENGONO INSERVIBILI** [TC MSP02].
     * * Restituisce **ISTANZE (Singleton) DIFFERENTI AL VARIARE DEI PARAMETRI FORNITI** [TC MSP03].
     * 
     */
    static async get(params, debug=false, _logger=null, logLevel=0) {
        try {
            let logger=null;
            let strParams = JSON.stringify(params);
            // Una istanza del logger è stata fornita con l'interfaccia error e debug
            if (!_logger || _logger.error === undefined || _logger.debug === undefined) {
                console.warn (`WARNING! No logger given, or given with unvalid interface (missing ".error()" or ".debug()" methods) -> Using Console`);
                logger = console; 
            } else {
                logger = _logger;
            };
            let instance = mysqlSingletonPool.instances.get(strParams);

            if (instance != undefined) {
                instance.__copies++;
                if (debug) logger.debug(`mysqlSingletonPool.get() - In uso istanza precedente (copia #${instance.__copies}): ${mysqlSingletonPool.instances.get(params).poolName}`)
                return instance;
            }
            let pool = await mysql.createPool(params);

            // Genera la versione promised del metodo getConnection(), per utilizzarlo nei test.
            pool.getConnPromised = nodeUtil.promisify(pool.getConnection)

            // Verifica la bontà della connessione
            if (await this.testConnection(pool, debug, logger)){
                
                // Trasmorma il metodo query per fargli restituire una
                // promise invece di accettare un callback
                if (debug) logger.debug(`mysqlSingletonPool.get() - Test connessione avvenuto con successo. Imposto il Singleton`)
                pool.query = nodeUtil.promisify(pool.query);
                pool.end = nodeUtil.promisify(pool.end);
                
                
                pool.poolName = Math.random().toString(36);
                
                // Assegna l'istanza della pool fissando il Singleton
                // Pool.instance = pool;

                // Assegna un nome
                // pool.poolName = Math.random().toString(36);
                // Pool.instance.params = params;

                if (debug) logger.debug(`mysqlSingletonPool.get() - Creata NUOVA ISTANZA: ${pool.poolName}`)
                pool.on('acquire', function(connection) {
                    if (debug) logger.debug(`    Connessione acquisita: ${connection.threadId}: "${pool.poolName}"`)
                })
                pool.on('release', function(connection) {
                    if (debug) logger.debug(`    Connessione terminata: ${connection.threadId}: "${pool.poolName}"`)
                });
                pool.__copies = 0;
                mysqlSingletonPool.instances.set(strParams, pool)


            } else {
                if (debug) logger.debug(`mysqlSingletonPool.get() - Test connessione fallito`)
            }

                
            // };    
            
            return mysqlSingletonPool.instances.get(strParams)
        } catch (err) {
            if (debug) console.error(`mysqlSingletonPool.get() - Errore durante la creazione della pool al DB MYSQL: ${err.name}, ${err.message}`);
            // throw(err);
        }
    };



    /**
     * Verifica la connessione per la `pool` appena generata.
     * @private
     * @static
     * 
     * @param {object} pool Istanza della `pool` di cui verificare l'affidabilità.
     * @param {boolean} debug Se `true`, informazioni addizionali verranno loggate tramite l'oggetto fornito come `logger` (3° argomento) o, in assenza di questo, tramite la `console`.
     * @param {object} logger Oggetto da utilizzare per il logging (deve avere il metodo `.error({string})`). Se non fornito, in caso di debug attivo verrà utilizzata la `console`.
     * 
     * @async Promise(`err`, `boolean`)
     * 
     * @memberof mysqlSingletonPool
     */
    static async testConnection(pool, debug, logger) {
        try {
            let connection = await pool.getConnPromised();
            if (connection) connection.release();
            return true;
        } catch (err) {
            switch (err.code) {
                case 'PROTOCOL_CONNECTION_LOST':
                    if (debug) logger.error(`mysqlSingletonPool.testConnection(pool: ${typeof pool}, debug: Boolean = ${debug}, logger: ${typeof logger}) - Connection with Database was closed`);
                    // resolve(false);
                    break;
                case 'ER_CON_COUNT_ERROR':
                    if (debug) logger.error(`mysqlSingletonPool.testConnection(pool: ${typeof pool}, debug: Boolean = ${debug}, logger: ${typeof logger}) - Database has too many connections`);
                    // resolve(false);
                    break
                case 'ECONNREFUSED':
                    if (debug) logger.error(`mysqlSingletonPool.testConnection(pool: ${typeof pool}, debug: Boolean = ${debug}, logger: ${typeof logger}) - Database connection was refused`);
                    // resolve(false);
                    break;
                default:
                    if (debug) logger.error(`mysqlSingletonPool.testConnection(pool: ${typeof pool}, debug: Boolean = ${debug}, logger: ${typeof logger}) - ERROR from pool.getConnection(): ${err.code||'N/A'} - ${err.name}: "${err.message}"`)
                    // reject(err);
                    break;
                };
                return false
        };
    };
   
};

module.exports = mysqlSingletonPool;