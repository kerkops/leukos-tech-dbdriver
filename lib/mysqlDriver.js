const DriverInterface = require('./driverInterface');

// const DbDriver = require('./dbDriver');
const singletonPool = require('./mysqlSingletonPool');

const mysqlUtils = require('./mysqlDriverUtils');

const jsutils = require('leukos-tech-jsutils');


const queryRes = require('./queryResult');
const okQuery = queryRes.QuerySuccess;
const koQuery = queryRes.QueryFail;

const paramsValidatorData = require("./poolParamsValidatorData");

/** Enumeration con i tipi noti di pacchetto MySql */
const MYSQL_packetTypes = Object.freeze({
    /** @property {String} ok Pacchetto di risposta per query svolte correttamente  */
    ok: 'OkPacket',
    /** @property {String} row Pacchetto dati per query di tipo *find()* */
    row: 'RowDataPacket',
});



/**  
 * Oggetto necessario a costruire la pool col server MySql. 
 * I parametri marcati con [M] sono obbligatori
 * @typedef {Object} PoolParams
 * 
 * @param {String} user Nome utente per l'accesso al Db [M]
 * @param {String} password Password per l'accesso al db [M]
 * @param {String} database Nome del database in uso [M]
 * 
 * @param {String} [host='localHost'] Nome dell'host ove risiede il db
 * @param {Number} [port=3306] Porta a cui connettersi
 * 
 * @param {any} localAddress (Optional) The source IP address to use for TCP connection. (Optional)
   @param {any} socketPath (Optional) The path to a unix domain socket to connect to. When used host and port are ignored.
 * 
 * @param {Number} [connectionLimit=10] Numero massimo di connessioni consentite 
 * @param {Number} [queueLimit=0] Numero massimo delle connessioni in attesa prima di restituire un errore con _.getConnection()_. Se _0_ (default) non vi è limite.
 * @param {Number} [connectionTimeout=10000] ms di attesa max per la connessione iniziale al server MySql
 * @param {Number} [acquireTimeout=10000] ms di attesa max per l'acquisizione della connessione
 * @param {Boolean} [waitForConnections=true] Ritenta la connessione se _true_, solleva un callback di errore se _false_
 * 
 */


'';

/**
 * DbDriver per Database MySql.
 *
 * @class MySqlDriver
 * @extends {DbDriver}
 */
class MySqlDriver extends DriverInterface {

    /**
     * Costruttore dell'oggetto driver.
     * Inizializza un oggetto vuoto.
     * @param {Boolean} debug Se True fornisce informazioni di debug tramite il logger fornito o, se non fornito, tramite la console
     * @param {Object} logger Istanza dellìoggetto da utilizzare per il logging, che deve avere i metodi debug(), warn() e Error(). Se non fornito viene utilizzata la console.
     * @param {Number} loggerLevel Livello di logging minimo abilitato
     */
    constructor(debug=false, logger=null, loggerLevel=1) {
        
        super(debug, logger, loggerLevel);
        
        
        /** @type {String} Nome del Db */
        this._dbName = null;

        /** @type {Object} Pool per la connessione al db mysql */
        this._pool = null;

        this._debug(`Generato NUOVO OGGETTO DRIVER.`)
        
    };

    /** Restituisce *true* se l'attributo con nome del dataBase è stato valorizzato col nome del fornito nei parametri (avviene solo dopo che la query "USE <dbName>;" restituisce *OkPacket*) */
    databaseSelected() {return this._dbName != null}

    /** Restituisce true se l'oggetto per la pool è stato generato */
    poolEnabled() {return this._pool != null}

    /** Restituisce la stringa identificativa della pool in uso, altrimenti *null* */
    poolName() { if (this.poolEnabled()) return this._pool.poolName };

    /**
     * Elimina, nella table fornita come 1° argomento i record identificati dagli id specificati col 2° argomento
     *
     * @param {String} tableName
     * @param {Array} ids Array di id dei record da eliminare (valori nei campi di tipo "PRIMARY KEY" corrispondenti a quelli nell'array verranno eliminati dall'operazione).
     * @param {String} idKey Nome del campo identificativo del record (se diverso da "id" [DEFAULT])
     * 
     * @async Promise(*err*, *queryResult*)
     * 
     * **Attributi significativi di queryResult**
     * 
     * * `success` **{Boolean}** `true` Se almeno un elemento è stato eliminato.
     * * `message` **{String}** Se `success=false` può contenenere il motivo dell'insuccesso.
     * * `query` **{String}** Stringa utilizzata per la query 
     * * `parameters` **{Array}** Array di parametri utilizzati per la query
     * * `affected` **{Number}** Numero di elementi eliminati
     * 
     * @memberof MySqlDriver
     */
    async _delete(tableName, ids, idKey='id') {
        let cmd='', parameters=[];
        let header = `MySqlDriver._delete("${tableName}", ${ids}, idKey="${idKey}")`;
        try {
            
            [cmd, parameters] = mysqlUtils.cmd.RC_delete(tableName, ids);

            let res = await this._pool.query(cmd, parameters);
            
            this._debug(`[QUERY RESULT] ${header} : ${JSON.stringify(res)}`);

            // this._log('debug', `Nome costruttore di res: ${res.constructor.name}`)
            
            /**  
             * * _res_ è di tipo "OkPacket";
             * * _res.affectedRows_ è positivo (almeno uno eliminato)
             * 
            */
            if (res.constructor.name == MYSQL_packetTypes.ok && res.affectedRows) {
                
                // => OkQuery{}
                return okQuery(
                    res,
                    cmd,
                    parameters,
                    [],
                    res.affectedRows,
                    res.changedRows
                );
            };

            /** 
             * *res* è di altro tipo 
             * => KoQuery{}
             * */
            return koQuery(res.message, res, cmd, parameters);
            
        } catch (err) {

            let msg = `${header} [${err.name}:${err.message}] Impossibile eliminare il record. `;
            
            this._warn( msg);
            
            // => KoQuery{}
            return koQuery(msg, [], cmd, parameters);
            
        }
    };


    /**
     * Assicura l'esistenza della tavola selezionata come argomento.
     * Se non esiste la crea, secondo i parametri dello schema fornito come argomento.
     * @param {String} tableName Nome della table da creare
     * @param {Object} schema Struttura della table da creare
     
     * @async Promise (err, queryResult)
     * 
     * **Attributi significativi di queryResult**
     * 
     * * `success` **{Boolean}** `true` se la table esiste al termine dell'esecuzione del metodo
     * * `message` **{String}** Ragione dell'insuccesso se `message==false` 
     * * `data` **{Object}** Oggetto *raw* restituito dalla libreria per il database
     * * `query` **{String}** Stringa utilizzata per la query al DB o `undefined`
     * 
     * 
     */
    async _ensureTable(tableName, schema) {
        
        let header = `mysqlDriver._ensureTable(${tableName}, [schema])`;
        
        // LA TABLE ESISTE GIA'
        if (await this._TB_exists(tableName)) {
            this._log('debug', `${header}: TABLE GIA' ESISTENTE`)
            return okQuery('', []);
        };
        let error = null;
        try {
            // Il metodo solleva eccezioni se la validazione non viene passata
            let res= mysqlUtils.validateTableSchema(schema);
        } catch (err) {
            return koQuery(err.message, null, null, null);
            
        };

        // console.error("ENSURE TABLE PROSEGUE")
        // Ottiene la stringa della query
        let cmd = mysqlUtils.cmd.TB_create(tableName, schema);
        
        this._log('debug', `${header} - QUERY: "${cmd}".`)
        
        try {
            // Esegue la query
            let res = await this._pool.query(cmd);
            
            this._debug(`${header} - QUERY RESULT: "${JSON.stringify(res)}".`)
            
            return okQuery(
                res, // data
                cmd, // query string,
                [], // params
                [], // insertedIds
                0, // affected
                0 // changed
            ); 
            
        } catch (err) {
            
            let msg = `${header} - !QUERY ERROR! ${err.name}: "${err.message}"`
            
            this._error(err, msg);
            
            return koQuery(msg, [], cmd);

            
        };

        
        
    };




    /**
     * Trova uno o più record nella tavola indicata come primo argomento
     * @param {String} tableName Nome della tavola/schema su cui operare
     * @param {Array} keys Chiavi da utilizzare per la ricerca
     * @param {Array} values Valori per le chiavi (deve essere di lunghezza uguale a quella di 'keys')
     * @async Promise (err, queryResult).
     * 
     * **Attributi significativi di queryResult**
     * * *success*  **{Boolean}**   `true` se la query si è svolta senza sollevare eccezioni (non comporta la restituzione di risultati).
     * * *message*  **{String}**    Vuoto se `success==true`. Altrimenti contiene una stringa indicante la ragione dell'insuccesso.
     * * *data*     **{Array}**     Array dei record corrispondenti ai parametri forniti. Vuoto se nessun record corrisponde ai parametri.
     * * *query*    **{String}**    Stringa di query (SE NECESSARIA) utilizzata  per l'interrogazione al database.
     * * *params*   **{Array}**     Array di parametri utilizzati in associazione con la stringa di query per l'interrogazione al database.
     * * *affected* **{Number}**    Numero di risultati prodotti dalla query (data.length.
     *
     */
    async _find(tableName, keys=undefined, values=undefined) {
        
        let header = `MysqlDriver._find(tableName=${tableName}, keys=${keys}, values=${values})`;
        let res;
        // Ottiene la stringadi query e l'array di parametri (argomenti per pool.query())
        if (keys && keys.length != values.length) return koQuery(`la lunghezza degli argomenti keys (${keys.length}) e values (${values.length}) deve essere la medesima.`)
        
        let {query, parameters} = mysqlUtils.cmd.RC_find(tableName, keys, values);

            
        try {
            
            this._debug(`[QUERY] ${header} >>> ${query} (${parameters})`);
            
            // Esegue la chiamata asincrona alla pool
            res = await this._pool.query(query, parameters);

            this._debug(`[QUERY RESULT] ${header} ==> ${JSON.stringify(res)}`);
            
            // Restituisce QueryResult
            return okQuery( 
                res,                /**     .data           {Array}     Array dei risultati     */
                query,              /**     .query          {String}    Stringa di query        */
                parameters,         /**     .params         {Array}     Array di parametri      */
                [],                 /**     .insertedIds    {Array}                             */
                res.length,         /**     .affected       {Number}    Risultati ottenuti      */
                res.changedRows     /**     .changed        {Number}                            */
            )
            
        } catch (err) {

            let msg = ` [!QUERY ERROR!] ${header} - ${err.name}: "${err.message}"`;
            
            this._warn(msg);
            
            return koQuery(msg, [], query, parameters);    
        }
    };

    

    /**
     * Rende operativo il Driver per il Db Mysql.
     * 
     * - Genera l'oggetto pool. 
     * - Avvia la connessione.
     * - Testa la consistenza della connessione.
     * 
     * @param {PoolParams} poolParams Parametri per la pool mysql
     * 
     * @async Promise(err, Boolean), `true` se l'inizializzazione del driver avviene con succcesso
     * 
     * @memberof MySqlDriver
     * 
     * @throws {MissingArgError} Se l'oggetto params non contiene uno degli attributi .user, .password, .database.
     * @throws {WrongArgTypeError} Se l'oggetto params contiene le proprietà mandatory ma di type errato
     */
    async _init (_poolParams) {
        
        let header = `mysqlDriver._init(poolParams)`;
        
        let valid = jsutils.validators.validateObject(_poolParams, paramsValidatorData);

        if (!valid.success) {
            
            // Una proprietà è mancante
            if (valid.problem == "MISS") {
                throw new jsutils.custErrors.args.MissingArgError(valid.propertyName)
            // Una proprietà è del type errato
            } else {
                throw new jsutils.custErrors.args.WrongArgTypeError(valid.propertyName, valid.value, valid.expected)
            }
        }
        
        // Crea una copia indipendente dei parametri
        let poolParams = jsutils.io.copyObj(_poolParams); 

        this._debug(`${header} - 'poolParams' = ${JSON.stringify(poolParams, jsutils.json.safeData)}`);
        
        // Conterrà la pool appena istanziata
        let pool = null;
        
        // Estrapola il nome del db dai parametri
        let _dbName = poolParams.database;
        
        
        // Cancella attributo .database dai parametri
        poolParams.database = undefined;

        try {
            // Ottiene il singleton della pool
            pool = await singletonPool.get(poolParams, this._debugOn, this._logger);
        
        // ERRORE NELL'OTTENIMENTO DELLA POOL - Esecuzione interrotta
        } catch(err) {
            this._warn(`${header} - !INIZIALIZZAZIONE FALLITA! - POOL NON OTTENUTA - ${err.name} - ${err.message}`);

            return false
        }

        // POOL OTTENUTA REGOLARMENTE
        if (pool) {

            // Assegna la pool all'attributo privato
            if (this.poolEnabled()) {
                try {
                    let msg = ``;
                    let stopped = await this._stop();
                    if (stopped) {
                        msg = `${header} - ARRESTATA POOL PRECEDENTE`
                    } else {
                        msg = `${header} - IMPOSSIBILE ARRESTARE POOL PRECEDENTE`
                    }
                    this._debug(msg);
                    this._pool = null;
                } catch (err) {
                    this._warn(`${header} - ERRORE NEL TERMINARE POOL PRECEDENTE - ${err.name} - ${err.message}`)
                    
                }
            }
            this._pool = pool;
            // this._log('debug', `${header} - INIZIALIZZAZIONE IN CORSO - OTTENUTA POOL`)
            
            // STEP 2: Verifica esistenza del Database e sua selezione
            try {
                
                let res = await this._Db_ensure(_dbName) 
                
                if (res) {
                    this._debug(`${header} - INIZIALIZZAZIONE TERMINATA CON SUCCESSO`);
                    return true;

                } else {
                    this._warn(`${header} - !INIZIALIZZAZIONE FALLITA! - DATABASE NON SELEZIONATO`);
                    return false
                
                }
                
                
            } catch (err) {
                
                this._warn(`${header} - !INIZIALIZZAZIONE FALLITA! - ERRORE DURANTE LA VERIFICA/SELEZIONE DEL DATABASE - ${err.name} - ${err.message}`);
                return false
            }
        } else {
            this._warn(`${header} - !INIZIALIZZAZIONE FALLITA! - POOL NON OTTENUTA - Non resistuita da singletonPool.get())`);

            return false;
        }
    };

    /**
     * Si assicura che il database fornito come argomento esista e che sia selezionato come database in uso
     * 
     * @param {*} dbName Nome del database
     * @async Restituisce una *Promise* con arg (*err*, *Boolean*), ***true* se il DB è regolarmente in funzione**.
     * @memberof MySqlDriver
     */
    async _Db_ensure(dbName) {

        let header = `mysqlDriver._Db_ensure(${dbName})`;
        
        /** STEP 1: Verifica dell'esistenza del Db e, se non esiste, lo crea. */
        try {
           
            // DB ESISTE 
            if (await this._Db_exists(dbName)) {
                this._debug(`${header} - VERIFICA/CREAZIONE DB - ESISTE`)
                // [esecuzione prosegue] V

            // DB NON ESISTE
            } else { 

                // DB CREATO
                if (await this._Db_create(dbName)) {
                    this._debug(`${header} - VERIFICA/CREAZIONE DB - NON ESISTEVA: CREATO`)
                    // [esecuzione prosegue] V

                // FALLITA CREAZIONE DB
                } else {

                    // DB NON PRESENTE - Esecuzione arrestata X
                    this._warn(`${header} - VERIFICA/CREAZIONE DB - !!IMPOSSIBILE CREARE IL DB!! - mysqlDriver._Db_create("${dbName}") HA RESTITUITO false`);

                    this._dbName = null;

                    this._warn(`${header} - Attributo '_dbName' AZZERATO PRIMA DI RESTITURE false`);
    
                    return false;
                    // [esecuzione !!ARRESTATA!!] X
                }
            };
        
        } catch (err) {
            
            // DB NON PRESENTE - Esecuzione arrestata
            this._error(err, `${header} - VERIFICA/CREAZIONE DB - !!ERRORE!!`);
            
            this._dbName = null;

            this._warn(`${header} - Attributo '_dbName' AZZERATO PRIMA DI RESTITURE false`);

            return false
            // [esecuzione !!ARRESTATA!!] X
        };

        
        /** STEP 2: Selezione del Db */
        try {
            // SELEZIONATO DB
            if (await this._Db_use(dbName)) {
                this._debug(`${header} - DB IN USO`);
                
                // ATTRIBUTO _dbName VALORIZZATO <-----------------------------------------
                this._dbName = dbName;
                return true

            // DB NON SELEZIONATO
            } else {
                this._warn(`${header} - SELEZIONE DB - IMPOSSIBILE SELEZIONARE IL DB: mysqlDriver._Db_use("${dbName}") HA RESTITUITO false`);

                
            }
            
        // ERRORE MENTRE SELEZIONAVA DB 
        } catch (err) {
            this._error(err, `${header} - SELEZIONE DB - !!ERRORE!!`)
            

        }
         // ATTRIBUTO _dbName !!AZZERATO!!
        this._dbName = null;
        this._warn(`${header} - Attributo '_dbName' AZZERATO PRIMA DI RESTITURE false`);

        return false
            
        
        
    };


    /**
     * Crea il database indicato come argomento, se già non esiste.
     * @param {String} dbName Nome del database da creare (se già non esistente).
     * @async Promise con argomento (*err*, *boolean*) ***true* se il DB E' STATO CREATO**.
     * 
     */
    async _Db_create(dbName) {
        let header = `mysqlDriver._Db_create("${dbName}")`;
        try {
            let {query, parameters} = mysqlUtils.cmd.DB_create(dbName);
            let res = await this._pool.query(query, parameters);
            /** Server risponde con un OkPacket */
            if (res.constructor.name == MYSQL_packetTypes.ok) {
                return true;
            }

            return false
        } catch (err) {
            
            
            if (err.code=="ER_ACCESS_DENIED_ERROR") {
                this._error(err, `${header} - ACCESSO AL DB NEGATO`);
                return false
            }
            this._error(err, `${header} - ERRORE CREAZIONE DB"`)
            return false
        }
    };

    /**
     * Verifica l'esistenza del DB fornito come argomento.
     * 
     
     *
     * @param {String} dbName Nome del DB da verificare
     * @async Promise con argomento *QueryResult*
     * @memberof MySqlDriver
     */
    async _Db_exists(dbName) {

        let header = `mysqlDriver._Db_exists("${dbName}")`;
        try {

            let {query, parameters} = mysqlUtils.cmd.DBS_show();
            this._debug(`[QUERY] ${header} - "${query}" - Parameters: ${parameters}`)
            // this._log('debug', `mysqlDriver._Db_exists("${dbName}"). Chiavi oggetto _pool: ${Object.keys(this._pool)}`)
            // let res2 = await this._query(mysqlUtils.cmd.DBS_show())
            let res = await this._pool.query(query, parameters);

            this._debug(`[QUERY RESULT] ${header} - ${JSON.stringify(res)}`)
            if (res.constructor.name=="Array") {
            
                for(let i = 0; i < res.length; i++) {
                    
                    if (res[i].Database ==dbName) {
                        this._debug(`${header} - DB INDIVIDUATO`)
                        return true
                    }
                };
            }
            this._warn(`${header} - !DB NON INDIVIDUATO!`)
            return false
        
        } catch (err) {
            
            if (err.code==='ER_ACCESS_DENIED_ERROR') {
                this._error(err, `${header} - ACCESSO AL DB NEGATO - ${err.name}: ${err.message}`)
                
            } else {
                this._error(err, `${header} - ERRORE IMPREVISTO.  - ${err.name} - ${err.message}`);
            }
            return false
        }
    };

    /**
     * Seleziona il Database fornito come argomento per l'utilizzo.
     *
     * @param {*} dbName Nome del database da selezionare
     * @async Restituisce una *Promise(err, Boolean)*; ***true* se il DB è in uso al termine della funzione**.
     * @memberof MySqlDriver
     */
    async _Db_use(dbName) {
        let cmd = mysqlUtils.cmd.DB_use(dbName);
        try {
            
            let res = await this._pool.query(cmd);
            // this._log('debug', `Use cmd result: ${JSON.stringify(res)}`);
            if (res.constructor.name==MYSQL_packetTypes.ok) return true
            return false

        } catch (err) {
            this._error(err, `MySqlDriver._Db_use("${dbName}") - ERRORE DALLA QUERY`);
            
            return false;

            // return false;
        }
        
    };

    /**
     * Resistuisce una promise con Argomento QueryObject, la cui proprietà **.data** contiene **l'elenco dei campi della *table fornita come argomento***
     *
     * @param {String} tableName Nome della table di cui si desidera conoscere la struttura.
     * @async Restituisce una **Promise(*err*, *QueryResult*)**. **data** contiene l'elenco delle chiavi con le rispettive proprietà. **affected** il numero di chiavi della table.
     * @memberof MySqlDriver
     */
    async _Tb_getSchema(tableName) {

        let header = `mysqlDriver._Tb_getSchema("${tableName}")`;

        try {
        
            let {query, parameters} = mysqlUtils.cmd.TB_showSchema(tableName);
            let res = await this._pool.query(query);
            this._debug(`${header} - RAW QUERY RESULT: ${JSON.stringify(res)}`);
            return okQuery(res, query, parameters, [], res.length);
        
        } catch (err) {
            this._error(err, `${header} - Impossibile recuperare i dettagli per la table`);
            
            return koQuery(err.message)
            
        };

    };
    
    
    /**
     * Verifica se la table fornita come argomento esiste tra quelle del DB
     *
     * @param {String} tableName Nome della table da verificare
     * @async Promise(*err*, *Boolean*)
     * @memberof MySqlDriver
     */
    async _TB_exists(tableName){

        let header = `mysqlDriver._TB_exists("${tableName}")`;

        try {
            let res = await this._pool.query(mysqlUtils.cmd.TBS_show());
            this._debug(` [QUERY RESULT] ${header} - ${JSON.stringify(res)}`);

            if (res.constructor.name == "Array") {
                for (let item of res) {
                    if (item[`Tables_in_${this._dbName}`]==tableName) {
                        this._log('debug', `${header} - TABLE TROVATA`);
                        return true;
                    }
                }

            }

            this._warn(`${header} - !TABLE NON TROVATA! ${JSON.stringify(res)}`);
            return false
            
        } catch (err) {
            this._error(err, `${header} - !!ERRORE MENTRE CERCAVO LA TABLE!! - ${err.name} - ${err.message}`);
            return false
            
        }
    };

    /**
     * Termina la connessione al DataBase in modo ordinato.
     * 
     * @param {Boolean} force Se `true` arresta la pool anche se altre copie sono disponibili rendendole tutte inutilizzabili.
     *
     * @async `Promise(err, Boolean)` Risolta in `true` se il thread con la connessione al Db è stato terminato con successo.
     * @memberof MySqlDriver
     */
    
    async _stop(force=false) {
        try {
            if (force || this._pool.__copies == 0 ) {
                let ended = await this._pool.end();
                // this._pool = undefined;
                this._debug(`MySqlDriver._stop() - TERMINATA POOL - ${this._pool.poolName}`)
                return true    
            } else {
                this._pool.__copies--;
                return false;
    
            }
            
            
        } catch (err) {
            if (err.message==="Cannot read property 'end' of null") {

            } else if  (err.message==='Pool is closed.'){

            } else {
                this._error(err, `Impossibile chiudere pool MySql`);
            
            }
            return false

        }
    };


    /**
     * Inserisce un record nella tavola indicata come primo argomento
     * @param {String} tableName Nome della tavola su cui operare
     * @param {Array} keys Chiavi del record da inserire
     * @param {Array} values Valori da assegnare alle varie chiavi della tavola
     * @async Promise con argomento (*err*, *queryResult*)
     * 
     * **Proprietà significative di queryResult**
     * 
     * * *success*      **{Boolean}**   `true` se l'inserimento si è svolta senza sollevare eccezioni.
     * * *message*      **{String}**    Vuoto se `success==true`. Se `false` contiene una stringa indicante la ragione dell'insuccesso.
     * * *data*         **{OBject}**    Differente in base al tipo di Db Utilizzato. 
     *      1. Per MySql, contiene l'oggetto *raw* restituito dalla libreria **mysql** (OkPacket).
     * * *query* 	    **{String}**    Stringa (SE NECESSARIA) utilizzata  per l'inserimento del record.
     * * *params*       **{Array}**     Array di parametri utilizzati in associazione con la stringa di inserimento nel database.
     * * *insertedIds*  **{Array}**     Array degli id dei nuovi record inseriti.
     * * *affected*     **{Number}**    Numero di record inseriti (`insertedIds.length`).
    *
     */
    
    async _insert(tableName, keys, values) {
        let cmd = mysqlUtils.cmd.RC_insert(tableName, keys, values);
        let header = `MySqlDriver._insert("${tableName}", keys, values)`
        let res;
        try {
            res = await this._pool.query(cmd,  values);

            this._debug(`${header} RAW RESPONSE: ${JSON.stringify(res)}`)
            return okQuery(
                res,
                cmd,
                values,
                [res.insertId],
                res.affectedRows,
                res.changedRows
            )
        } catch (err) {
            this._warn(`[${err.name}:${err.message}] Impossibile inserire i dati nella table`)
            if (err.code == "ER_BAD_FIELD_ERROR:") {

            }
            return koQuery(
                `Inserimento del record impossibile - ${err.name} - ${err.message}`,
                [],
                cmd,
                values
            )
        }
    };

    /**
     * Aggiorna uno o più record per la tavola/schema indicata come primo argomento
     * @param {String} tableName Nome della tavola/schema su cui operare
     * @param {Array} recordIds Id dei record da aggiornare
     * @param {Array} keys Elenco delle chiavi il cui valore deve essere aggiornato
     * @param {Array} values Array di valori da assegnare alle chiavi (deve essere di lunghezza uguale a quella di 'keys')
     * @param {String} idKey Chiave da utilizzare per l'id (default="id")
     * 
     * @async Promise con argomento (*err*, *queryResult*)
     * 
     * **Proprietà significative di queryResult**
     * 
     * * `success`  **{Boolean}**   `true` se l'aggiornamento valori si è svolto senza sollevare eccezioni.
     * * `message`  **{String}**    Vuoto se `success=true`. Se `false` contiene una stringa indicante la ragione dell'insuccesso.
     * * `data`     **{Object}**    Oggetto *raw* restituito dalla libreria per la comunicazione col DB (per `mysql` è `OkPacket`). 
     * * `query` 	**{String}**    Stringa (SE NECESSARIA) utilizzata  per l'inserimento del record.
     * * `params`   **{Array}**     Array di parametri utilizzati in associazione con la stringa di inserimento nel database.
     * * `changed`  **{Number}**    Numero dei record modificaty dalla query.
     */
    async _update(tableName, recordIds, keys, values, idKey="id") {
        let cmd='', parameters=[], header;
        
        try {
            header = `MySqlDriver._update("${tableName}", ${recordIds}, ${keys}, ${values})`;

            [cmd, parameters] = mysqlUtils.cmd.RC_update(tableName, recordIds, keys, values, idKey);
            
            this._debug(`[QUERY] ${header} >>> ${cmd} (${parameters.join(',')})`);
            
            let res = await this._pool.query(cmd, parameters);

            this._debug(`[QUERY RESULT] ${header}: ${JSON.stringify(res)}`)

            if (res.affectedRows>0) {
                return okQuery(
                    res,
                    cmd,
                    parameters,
                    [],
                    res.affectedRows,
                    res.changedRows
                )    
            }
            return koQuery('WRONG ID', res, cmd, parameters);
            
        } catch (err) {
            this._error(err, `[!QUERY ERROR!] ${header}`);

            return koQuery(`[${err.name}:${err.message}] Aggiornamento record impossibile`, [], cmd, parameters);
            
        }
    };

    
    
        
};

module.exports = MySqlDriver;
