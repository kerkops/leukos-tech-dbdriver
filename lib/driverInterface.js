
let {ancestors} = require('leukos-tech-jsutils');

/**
 * Oggetto per i dettagli dei risultati delle interrogazioni al database
 * @typedef {Object} QueryOperationDetails
 * @param {Number} inserted Numero di record INSERITI nel Db
 * @param {Number} updated Numero di record AGGIORNATI nel Db
 * @param {Number} deleted Numero di record ELIMINATI nel Db
 */
'';

/**
 * Oggetto per i risultati delle interrogazioni al DataBase
 * @typedef {Object} QueryResult
 * @param {Boolean} success Esito operazione sul db
 * @param {String} message Messagio accompagnato ad operazioni con esito negativo
 * @param {String} query Stringa di query utilizzata
 * @param {Array} parameters Array di parametri utilizzati
 * @param {Array} data Array dei risultati prodotti dalla query
 * @param {Number} affected Numero dei record coinvolti attivamente nell'operazione
 * @param {Array} insertedId Contiene gli id dei campi aggiunti
 * @param {Number} changed Numero dei record modificati con l'operazione
 */
'';

/**
 * Definisce i metodi pubblici comuni a tutti i driver per db
 *
 * @class DbDriverInterface
 * @interface
 */
class DbDriverInterface extends ancestors.LoggedClass {
    
    /**
     * Elimina uno o più record dalla tavola/schema indicata come primo argomento
     * @param {String} tableName Nome della tavola/schema su cui operare
     * @param {String} ids Id dei record da eliminare
     * 
     * @async Promise(*err*, *queryResult*)
     * 
     * L'oggetto `queryResult` contiene le seguenti proprietà:
     * 
     * * `success` **{Boolean}** `true` Se almeno un elemento è stato eliminato.
     * * `message` **{String}** Se `success=false` può contenenere il motivo dell'insuccesso.
     * * `query` **{String}** Stringa utilizzata per la query 
     * * `parameters` **{Array}** Array di parametri utilizzati per la query
     * * `affected` **{Number}** Numero di elementi eliminati
     */
    async delete(tableName, ids) {
        return this._delete(tableName, ids);
    };


    /**
     * Si assicura l'esistenza di una determinata tavola nel db
     * @param {String} tableName Nome della table da creare
     * @param {Object} schema Oggetto indicante la struttura della table/schema da creare
     * 
     * @async Promise (err, queryResult)
     * 
     * L'oggetto `queryResult` contiene le seguenti proprietà:
     * 
     * * `success` **{Boolean}** `true` se la table esiste al termine dell'esecuzione del metodo
     * * `message` **{String}** Ragione dell'insuccesso se `message==false` 
     * * `data` **{Object}** Oggetto *raw* restituito dalla libreria per il database
     * * `query` **{String}** Stringa utilizzata per la query al DB o `undefined`
     * 
     * 
     * 
     * Schema:
     * - [
     * -        {
     * -        keyName: String,
     * -        type:String, 
     * -        size:Number(optional), 
     *          isUnsigned: Boolean,
     * -        autoIncrement: Boolean,
     * -        notNull: Boolean,
     * -        isPrimary: Boolean,
     * -        isIndex: Boolean,
     * -        isUnique: Boolean,
     * -        isReference: Boolean,
     * -        reference: 
     * -            {
     * -            tableName: String,
     * -            tableField: String,
     * -            onUpdate: String,
     * -            onDelete: String, (CASCADE, DELETE, RESTRICT)
     * -            }
     * -        },
     *          ... 
     * - ]
     */
    async ensureTable(tableName, schema) {
        return this._ensureTable(tableName, schema);
    };


    /**
     * Trova uno o più record nella tavola indicata come primo argomento
     * @param {String} tableName Nome della tavola/schema su cui operare
     * @param {Array} keys Chiavi da utilizzare per la ricerca
     * @param {Array} values Valori per le chiavi (deve essere di lunghezza uguale a quella di 'keys')
     * @async Promise (err, queryResult).
     * 
     * L'oggetto `queryResult` contiene le seguenti proprietà:
     * 
     * * `success`  **{Boolean}**   `true` se la query si è svolta senza sollevare eccezioni (non comporta la restituzione di risultati).
     * * `message`  **{String}**    Vuoto se `success==true`. Altrimenti contiene una stringa indicante la ragione dell'insuccesso.
     * * `data`     **{Array}**     Array dei record corrispondenti ai parametri forniti. Vuoto se nessun record corrisponde ai parametri.
     * * `query`    **{String}**    Stringa di query (SE NECESSARIA) utilizzata  per l'interrogazione al database.
     * * `params`   **{Array}**     Array di parametri utilizzati in associazione con la stringa di query per l'interrogazione al database.
     * * `affected` **{Number}**    Numero di risultati prodotti dalla query (data.length.
     *
     */
    async find(tableName, keys, values) {
        return this._find(tableName, keys, values);
        
    };


    /**
     *Inizializza il driver secondo i parametri forniti
     *
     * @param {object} params Oggetto con i parametri necessari a rendere operativo il database
     * 
     * @async Promise(err, Boolean), `true` se l'inizializzazione del driver avviene con succcesso
     * 
     * @memberof DbDriverInterface
     */
    async init(params) {
        return this._init(params);
    };
    

    /**
     * Inserisce un record nella tavola indicata come primo argomento
     * @param {String} tableName Nome della tavola su cui operare
     * @param {Array} keys Chiavi del record da inserire
     * @param {Array} values Valori da assegnare alle varie chiavi della tavola
     * @async Promise con argomento (*err*, *queryResult*)
     * 
     * L'oggetto `queryResult` contiene le seguenti proprietà:
     * 
     * * `success`      **{Boolean}**   `true` se l'inserimento si è svolta senza sollevare eccezioni.
     * * `message`      **{String}**    Vuoto se `success==true`. Se `false` contiene una stringa indicante la ragione dell'insuccesso.
     * * `data`         **{OBject}**    Differente in base al tipo di Db Utilizzato. 
     *      1. Per MySql, contiene l'oggetto *raw* restituito dalla libreria **mysql** (OkPacket).
     * * `query` 	    **{String}**    Stringa (SE NECESSARIA) utilizzata  per l'inserimento del record.
     * * `params`       **{Array}**     Array di parametri utilizzati in associazione con la stringa di inserimento nel database.
     * * `insertedIds`  **{Array}**     Array degli id dei nuovi record inseriti.
     * * `affected`     **{Number}**    Numero di record inseriti (`insertedIds.length`).
     *
     */
    async insert(tableName, keys, values) {
        return this._insert(tableName, keys, values);
    };

    /**
     * Termina la connessione al DataBase in modo ordinato.
     * 
     * **Dalla versione 0.2.0 questo metodo rispetta i seguenti criteri: ogni volta che viene richiesta una *copia* del Singleton, viene incrementato un contatore. La chiamata a questo metodo, se il contatore delle copie è > 0, si limita a decremetarne il valore. Se == 0 arresta la *pool* rendendo l'oggetto inutilizzabile per comunicare col database**
     * 
     * @param {Boolean} force Se `true` arresta immediatamente la pool rendendo inutilizzabile qualsiasi copia del Singleton. Altrimenti decrementa il contatore delle copie in uso in caso sia > 0 e arresta la pool solo se il contatore è 0.
     * 
     * @async Promise (err, boolean) `true` se la pool è stata arrestata effettivamente, `false` in caso di errori o, dalla ver. 0.2.0, se il contatore delle copie è > 0.
     * 
     * @memberof DbDriverInterface
     */
    async stop(force=false) {
        return await this._stop(force)
    }

    
    /**
     * Aggiorna uno o più record per la tavola/schema indicata come primo argomento
     * @param {String} tableName Nome della tavola/schema su cui operare
     * @param {Array} recordIds Id dei record da aggiornare
     * @param {Array} keys Elenco delle chiavi il cui valore deve essere aggiornato
     * @param {Array} values Array di valori da assegnare alle chiavi (deve essere di lunghezza uguale a quella di 'keys')
     * @param {String} idKey Chiave da utilizzare per l'id (default="id")
     * 
     * @async Promise con argomento (*err*, *queryResult*)
     *  
     * L'oggetto `queryResult` contiene le seguenti proprietà:
     * 
     * * `success`  **{Boolean}**   `true` se l'aggiornamento valori si è svolto senza sollevare eccezioni.
     * * `message`  **{String}**    Vuoto se `success=true`. Se `false` contiene una stringa indicante la ragione dell'insuccesso.
     * * `data`     **{Object}**    Oggetto *raw* restituito dalla libreria per la comunicazione col DB (per `mysql` è `OkPacket`). 
     * * `query` 	**{String}**    Stringa (SE NECESSARIA) utilizzata  per l'inserimento del record.
     * * `params`   **{Array}**     Array di parametri utilizzati in associazione con la stringa di inserimento nel database.
     * * `changed`  **{Number}**    Numero dei record modificaty dalla query.
     */

    update(tableName, recordIds, keys, values, idKey="id") {
        return this._update(tableName, recordIds, keys, values, idKey);
    };
    

    
    /** [Da reimplementare nelle classi che realizzano l'interfaccia] */
    async _delete(tableName, ids) {this._notImplementedWarning('_delete(tableName, ids)'); };

    /** [Da reimplementare nelle classi che realizzano l'interfaccia] */
    async _ensureTable(tableName, schema) {this._notImplementedWarning(`_createTable(tableName, schema)`); };

    /** [Da reimplementare nelle classi che realizzano l'interfaccia] */
    async _find(tableName, keys, values) {this._notImplementedWarning('_find(tableName, keys, values)'); };

    /** [Da reimplementare nelle classi che realizzano l'interfaccia] */
    async _init(params) {this._notImplementedWarning('start(params)'); };

    /** [Da reimplementare nelle classi che realizzano l'interfaccia] */
    async _insert(tableName, keys, values) {this._notImplementedWarning('_insert(tableName, values)'); };

    /** [Da reimplementare nelle classi che realizzano l'interfaccia] */
    async _stop() {this._notImplementedWarning('_stop()'); };

    /** [Da reimplementare nelle classi che realizzano l'interfaccia] */
    async _update(tableName, recordIds, keys, values, idKey="id") {this._notImplementedWarning('_update(recordsIds, keys, values)'); };


    /**
     * Genera un warning relativo ai metodi astratti dell'interfaccia non re-implementati dalle classi che la ereditano
     *
     * @param {string} [fName='UNKNOWN METHOD NAME'] Signature della funzione da segnalare
     
     * @memberof DbDriverInterface
     */
    _notImplementedWarning (fName='UNKNOWN METHOD NAME') {this._log('warn', `!NON IMPLEMENTATO! Metodo '${this.constructor.name}.${fName}'` ); };

};


module.exports = DbDriverInterface;



