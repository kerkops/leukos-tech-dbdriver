

const mysqlDriver = require ('./lib/mysqlDriver');
const queryObjects = require('./lib/queryResult');

/** @constant { String [ ] } DataBase correntemente supportati dal driver */
const SUPPORTED_DATABASES = [ 'mysql' ];


/**
 * Genera e restituisce l'istanza del driver adatta al tipo di DataBase specificato dall'argomento `type`.
 *
 * @param {string} type Tipo di database con cui il driver andrà a dialogare
 * @param {boolean} debug Se `true` informazioni sull'esecuzione verranno loggate tramite l'oggetto fornito con `logger`. (Default `false`)
 * @param {object} logger Oggetto con cui effettuare il logging se `debug` fornito \`true\`
 * @param {Number} logLevel Livello di logging minimo consentito (default 0)
 * 
 * ---
 * 
 * @throws {TypeError} Se la stringa fornita con \`type\` non figura tra i database supportati (elencati in `leukos-tech-dbDriver.SUPPORTED_DATABASES`)
 * 
 * ---
 * 
 * @returns {object} Oggetto discendente di `dbDriver` adatto al dialogo con il tipo di database specificato con `type`.
 */
function dbFactory (type, debug=false, logger=console, logLevel=1) {

    // Argomento `type` non valido
    if (!SUPPORTED_DATABASES.includes(type)) {
        throw new TypeError(`leukos-tech-db-driver - Tipo di Database non supportato \`${type}\``);
    };

    switch (type.toString().toLowerCase()) {
        case 'mysql':
            return new mysqlDriver(debug, logger, logLevel)
    }
};
/** @constant { String[] } DataBase correntemente supportati dal driver */
dbFactory.SUPPORTED_DATABASES = SUPPORTED_DATABASES;

module.exports = {
    driverFactory: dbFactory,
    queryResult: queryObjects.QueryResult,
    querySuccess: queryObjects.QuerySuccess,
    queryFail: queryObjects.QueryFail
}
