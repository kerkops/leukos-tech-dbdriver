# Leukos-Tech-DbDriver

##  Descrizione

Il package è stato sviluppato internamente per fornire una interfaccia strandard e facilmente adattabile verso le varie librerie di gestione dei database di `NodeJs`.


## <a name='summary'></a>Sommario

* [Installazione](#install)
* [Utilizzo del package](#usage)
* [Metodi della classe Leukos-Tech-DbDriver](#methods)
    * [DbDriver.init(params)](#init)
    * [DbDriver.ensureTable(tableName, schema)](#ensure)
    * [DbDriver.insert(tableName, keys, values)](#insert)
    * [DbDriver.find(tableName, keys, values)](#find)
    * [DbDriver.update(tableName, ids, keys, values, idkey="id")](#update)
    * [DbDriver.delete(tableName, ids, idKey="id")](#delete)
    * [DbDriver.stop(force=false)](#stop)


## <a name='install'></a> Installazione

Nella directory principale del tuo progetto digita:

```bash
~/your/project/dir$ npm install leukos-tech-dbdriver --save
```
<br>

## <a name='usage'></a> Utilizzo del package



#### Oggetto reso dall'*import* del package

L'*import* restituisce un oggetto che espone i seguenti metodi (statici):

```node
leukos-tech-dbdriver.driverFactory(type, debug, logger)
```
Factory per generare l'istanza del *driver*.
<br>


```typescript
leukos-tech-dbdriver.queryResult(
    success {boolean}, 
    message {string} = "", 
    data {Array} = [], 
    query {string} = "", 
    params {Array} = [], 
    insertedIds {Array} = [], 
    affected {number} = 0, 
    changed {number} = 0 
)
```
Metodo statico per generare l'oggetto `QueryResult` utilizzato come data exchange object.
<br>

```typescript
leukos-tech-dbdriver.querySuccess(
    data {Array} = [], 
    query {string} = "", 
    params {Array} = [], 
    insertedIds {Array} = [], 
    affected {number} = 0, 
    changed {number} = 0 
)
```
Metodo (statico) di convenienza per generare un oggetto `QueryResult` con argomento `success==true` e `message` vuoto.
<br>

```typescript
leukos-tech-dbdriver.queryFail(
    message {string} = "", 
    data {Array} = [], 
    query {string} = "", 
    params {Array} = [], 
    insertedIds {Array} = [], 
    affected {number} = 0, 
    changed {number} = 0 
)
```
Funzione di convenienza per generare un oggetto `QueryResult` con argomento `success=false`.

<br>

#### Accesso alla *factory* del driver

**Metodo 1**

```node
const driverFactory = require('leukos-tech-dbdriver').driverFactory;
```
**Metodo 2**

```node   
const {driverFactory} = require('leukos-tech-dbdriver');
```

**Metodo 3**

```node
const ltDbDriver = require('leukos-tech-dbdriver');
const driverFacotry = ltDbdriver.driverFactory;`
```
<br>
#### Creazione del driver tramite la *factory*

Tramite la *factory* ottenuta il driver viene generato fornendo a quest'ultima il *tipo* di driver che si desidera generare e poi, opzionalmente, se attivare il *debug* e quale oggetto utilizzare per il *logging*.

```node
let dbDriver = driverFactory(type, debug, logger);
```

*(i valori supportati per l'*arg* `type` sono contenuti in `driverFactory.SUPPORTED_DATABASES`)*

Appena generato dalla *factory* l'oggetto `dbDriver` contiene tutti i metodi, ma le sue proprietà fondamentali (variabili in base al `type` del driver) non sono ancora state valorizzate: è dunque ancora inutilizzabile. 
**Per attivare l'oggetto è necessario eseguire il metodo `dbDriver.init(params)`** ([Vai alla documentazione](#init)).

<br>

####  <a name=newstop></a>Arresto del driver

Qualora fosse necessario, al termina dell'utilizzo del driver l'oggetto va fermato col metodo `.stop(force)`:

```node
let driverStopped = await dbDriver.stop()
```

**(dalla versione 0.2.0 il comportamento di questo metodo è stato modificato: per ogni copia del Singleton richiesta esso va chiamato, consentendo di chiudere effettivamente la connessione al dataBase soltanto alla chiamata da parte dell'ultimo)**

es. Si ipotizzi di avere due moduli differenti che importano il package e lo inizializzano con gli stessi parametri

**`module1.js`**

```node
const {driverFactory} = require('leukos-tech-dbdriver');
...
let dbDriver_copy1 = driverFactory(type, debugEnabled, logger, logLevel);
let initDone_copy1 = dbDriver_copy1.init(commonParameters);
```

**`module2.js`**

```node
const {driverFactory} = require('leukos-tech-dbdriver');
...
let dbDriver_copy2 = driverFactory(type, debugEnabled, logger, logLevel);
let initDone_copy2 = dbDriver_copy2.init(commonParameters);
```

Si avranno a questo punto 2 copie del medesimo Singleton per l'interazione al database (i parametri sono condivisi). 
Effettuando, in **`module1.js`**, la chiamata:

```node
let driver_stopped = await dbDriver_copy1.stop();
```
la variabile `driver_stopped` viene risolta in `false` e il Singleton è ancora utilizzabile (per il `module2`). 

Ma se dopo di ciò, in **`module2.js`** viene eseguita la chiamata:

```node
let driver_stopped = await dbDriver_copy2.stop();
```

la variabile `driver_stopped` viene ora risolta in `true` e la connessione al DataBase effettivamente chiusa.

Questo comportamento può essere aggirato fornendo l'argomento `force` con valore `true`.

Se la chiamata, nel **`module1.js`** fosse:

```node
let driver_stopped = dbDriver_copy1.stop(true);
```

la connesione verrebbe chiusa immediatamente, anche se altre istanze del driver sono in uso presso altri moduli (rendendo inservibili anche queste ultime).

<br>

###  Utilizzo dei metodi

Tutti i metodi del driver restituiscono una *Promise*. E' possibile quindi utilizzarli con la notazione standard 

```node
Driver.<metodo>(...args)
.then( (queryResult) => { ... } )
.catch( (err) => { ... } );
```


o con la notazione *async/await*
```node
try {
    let queryResult = await Driver.<metodo>(...args);
    ...
} catch (err) {
    ...
};
```


[(Torna al Sommario)](#summary)

---
<br>




# <a name='methods'></a>Metodi della classe Leukos-Tech-DbDriver


##  <a name='init'></a>`DbDriver.init( params {Object} )`

 Inizializza il driver secondo i parametri forniti come argomento.

###  Argomenti

* `params` {**params**} 	Oggetto con i parametri necessari a rendere operativo il database.


###  Tipi restituiti

`Promise(err, Boolean)`

Risolta in `true` se l'inizializzazione del driver è andata a buon fine coi parametri forniti.

#### Requisiti dell'Oggetto *params*:

L'oggetto *params* deve contenere **necessariamente** le  seguenti proprietà: 

* `user` {**string**}   Username per l'accesso al database.
* `password` {**string**} 	Password per l'accesso al database.
* `database` {**string**} 	Nome del database che si desidera utilizzare.

Se le proprietà `params.host` {**String**} e `params.port` {**Number**} non vengono specificate, sono utilizzate quelle di default (*"localHost"* per il server e es. per MySql *3306*).


#### Test Cases

* **INI01**  - Si risolve in `true` quando dei parametri corretti sono forniti al metodo consentendo l'avvio della connessione al database.

* **INI02** -  Solleva `Leukos-Tech-JsUtils.custErrors.args.MissingArgError` se le proprietà `params.database` {**String**}, `params.user` {**String**}, `params.password` {**String**} sono *`undefined`*.


* **INI03** - Solleva `Leukos-Tech-JsUtils.custErrors.args.WrongArgTypeError` se le proprietà `params.database` {**String**}, `params.user` {**String**}, `params.password` {**String**} non sono di tipo `String`.

* **INI04** - Si risolve `false` se le proprietà `params.user` o `params.password` non equivalgono a credenziali corrette.

* **INI05** - Si risolve in `true` e restituisce Singleton del driver tra loro correlati se inizializzato con oggetti (indipendenti) contenenti gli stessi valori.

* **INI06** - Si risolve in `false` se fornito un valore errato per `host` o `port`

[(Torna al Sommario)](#summary)

---


## <a name='ensure'></a>`DbDriver.ensureTable( tableName {String}, schema {TableSchema} )`

Assicura l'esistenza della *table* specificata come 1° argomento.

**Se non esiste la crea secondo i parametri specificati col 2° argomento.**

###  Argomenti

* `tableName` {**String**} Nome della *table* di cui verificare l'esistenza
* `schema` {**TableSchema**} Array di oggetti *TableField* con cui specificare le proprietà dei vari campi dei record della table corrente (v. sotto).

###  Tipi restituiti

`Promise(err, queryResult)`

L'oggetto `queryResult` contiene le seguenti proprietà:

* `success` **{Boolean}** `true` se la table esiste al termine dell'esecuzione del metodo
* `message` **{String}** Ragione dell'insuccesso se `message==false` 
* `data` **{Object}** Oggetto *raw* restituito dalla libreria per il database
* `query` **{String}** Stringa utilizzata per la query al DB o `undefined`

####  Oggetto *TableSchema*

E' un array di oggetti, ciascuno dei quali contiene le proprietà da assegnare ai diversi campi da creare nella tavola. Ogni oggetto deve avere la seguente struttura:


* `keyName:        STRING,` 
* `type:           STRING, `
* `size:           NUMBER,` 
* `isUnsigned:     BOOLEAN,`
* `autoIncrement:  BOOLEAN,`
* `notNull:        BOOLEAN,`
* `isPrimary:      BOOLEAN,`
* `isIndex:        BOOLEAN,`
* `isUnique:       BOOLEAN,`
* `isReference:    BOOLEAN,`
* `reference: {tableName:  STRING, tableField: STRING}`

### Test Cases

* **ENS01** - Si risolve in `queryResult.success:true` dopo aver creato la table, se forniti parametri conformi.

* **ENS02** - Si risolve in `queryResult.success:true` anche se chiamato per una `table` già esistente, se forniti parametri conformi.

* **ENS03** - Si risolve in `queryResults.success: false` e non crea la table se `schema` non possiede la giusta formattazione (non passa la validazione).

[(Torna al Sommario)](#summary)

---

## <a name='insert'></a> `DbDriver.insert( tableName {String}, keys {Array}, values{Array} )`

Inserisce *1 record* nella tavola indicata come primo argomento


###  Argomenti

* `tableName` {**String**}  Nome della tavola/schema su cui operare
* `keys` {**Array**}  Chiavi del record da inserire
* `values`  {**Array**}  Valori valori per il nuovo record da inserire

###  Tipi restituiti

`Promise(err, queryResult)`

L'oggetto `queryResult` contiene le seguenti proprietà:

* `success`      **{Boolean}**   `true` se l'inserimento si è svolta senza sollevare eccezioni.
* `message`      **{String}**    Vuoto se `success==true`. Se `false` contiene una stringa indicante la ragione dell'insuccesso.
* `data`         **{Object}**    Oggetto *raw* restituito dalla libreria utilizzata come interfaccia al DB.
* `query` 	    **{String}**    Stringa (SE NECESSARIA) utilizzata  per l'inserimento del record.
* `params`       **{Array}**     Array di parametri utilizzati in associazione con la stringa di inserimento nel database.
* `insertedIds`  **{Array}**     Array degli id dei nuovi record inseriti.
* `affected`     **{Number}**    Numero di record inseriti (`insertedIds.length`).

###  Utilizzo

E' un metodo asincrono che restituisce una `Promise(err, arg)`. 
Può essere quindi usato con la notazione standard 

```node
Driver.insert(
    "tableName",                           // Nome della table
    ["key1", "key2", "key3", ...],         // Array con i nomi delle chiavi 
    ["value1", "value2", "value3", ...]    // Array coi valori da assegnare ai rispettivi campi
)
.then( (queryResult) => { ... } )
.catch( (err) => { ... } );
```

o con la notazione *async/await*

```node
try {
    let queryResult = await Driver.insert(
        "tableName",                         // Nome della table
        ["key1", "key2", "key3", ...],       // Array con i nomi delle chiavi
        ["value1", "value2", "value3", ...]  // Array coi valori da assegnare ai rispettivi campi
    );
    ...
} catch (err) {
    ...
};
```

###  Test Cases

* **INS01** - Risolve in `queryResult.success: true` ed inserisce il record con argomenti conformi (campo id - AUTO_INCREMENT - non fornito).

* **INS02** - Risolve in `queryResult.success: false` e NON inserisce il record con con argomento 'keys' non valido (chiavi errate).

* **INS03** - Risolve in `queryResult.success: true` ed inserisce il record con argomenti conformi (campo id - AUTO_INCREMENT - fornito).

* **INS04** - Risolve in `queryResult.success: false` e NON inserisce il record con con argomento 'values' non valido (numero di elementi errato).  

[(Torna al Sommario)](#summary)

---

##  <a name='find'></a>`DbDriver.find( tableName {String}, keys {Array}, values{Array} )`

###  Argomenti

* `tableName`   {**String**}    Nome della tavola/schema su cui operare
* `keys`        {**Array**}     Chiavi da utilizzare per la ricerca
* `values`      {**Array**}     Valori per le chiavi (deve essere di lunghezza uguale a quella di 'keys')

###  Tipi Restituiti

`Promise(err, queryResult)`

L'oggetto `queryResult` contiene le seguenti proprietà:

* `success`  **{Boolean}**   `true` se la query si è svolta senza sollevare eccezioni (non comporta la restituzione di risultati).
* `message`  **{String}**    Vuoto se `success==true`. Altrimenti contiene una stringa indicante la ragione dell'insuccesso.
* `data`     **{Array}**     Array dei record corrispondenti ai parametri forniti. **Vuoto se nessun record corrisponde ai parametri.**
* `query`    **{String}**    Stringa di query (SE NECESSARIA) utilizzata  per l'interrogazione al database.
* `params`   **{Array}**     Array di parametri utilizzati in associazione con la stringa di query per l'interrogazione al database.
* `affected` **{Number}**    Numero di risultati prodotti dalla query (`data.length`)

###  Utilizzo

E' un metodo asincrono che restituisce una promise. Può essere quindi usato con la notazione standard 

```node
Driver.find(tableName, [ "field_name" ], [ "value" ] )
.then( (queryResult) => { ... } )
.catch( (err) => { ... } );
```

o con la notazione *async/await*

```node
try {
    let queryResult = await Driver.find("aTableName", ["fildName1"], ["aValue"]);
    ...
} catch (err) {
    ...
};
```

###  Test Cases

* **FIN01** - Risolve in `queryResult.success:true` e `queryResult.data` contiene i record attesi con valori conformi.

* **FIN02** - Risolve in `queryResult.success:true` e `queryResult.data` contiene tutti i record della table se chiamato senza argomenti (eccetto `tableName`).

* **FIN03** - Risolve in `queryResult.success:false` se `keys` e `values` sono Array di lunghezza diversa.

* **FIN04** - Risolve in `queryResult.success:true` e `queryResult.data` contiene i record attesi con valori conformi (campi multipli diversi da `id`).

[(Torna al Sommario)](#summary)

---

## <a name='update'></a> `DbDriver.update( tableName {String}, ids {Array}, keys {Array}, values {Array}, idKey {String} = "id" )`

Aggiorna *1 o più record* nella tavola indicata come primo argomento, con i valori specificati


###  Argomenti

* `tableName` {**String**}  Nome della tavola/schema su cui operare.
* `recordIds` {**Array**} Array di id per i record da aggiornare.
* `keys` {**Array**}  Chiavi del/i record da aggiornare.
* `values`  {**Array**}  Valori per il/i record da aggiornare, da assegnare alle rispettive chiavi (stesso indice).
* `idKey` {**String**} Chiave da utilizzare per il campo *id* dei record (default = `"id"`)

###  Tipi restituiti

`Promise(err, queryResult)`

L'oggetto `queryResult` contiene le seguenti proprietà:

* `success`  **{Boolean}**   `true` se l'aggiornamento valori si è svolto senza sollevare eccezioni.
* `message`  **{String}**    Vuoto se `success=true`. Se `false` contiene una stringa indicante la ragione dell'insuccesso.
* `data`     **{Object}**    Oggetto *raw* restituito dalla libreria per la comunicazione col DB (per `mysql` è `OkPacket`). 
* `query` 	 **{String}**    Stringa (SE NECESSARIA) utilizzata  per l'inserimento del record.
* `params`   **{Array}**     Array di parametri utilizzati in associazione con la stringa di inserimento nel database.
* `changed`  **{Number}**    Numero dei record modificaty dalla query.

### Utilizzo

E' un metodo asincrono che restituisce una *Promise*. Può essere quindi usato con la notazione standard 

```node
Driver.update(`
    "tableName",                // Nome della table
    [ 1 ],                      // Array con gli id da aggiornare
    [ "key1", "key2", ... ],    // array con i nomi delle chiavi (*dbSchema[i].keyName*).
    [ "value1", "value2", ... ] // Array coi nuovi valori da assegnare ai rispettivi campi.
)
.then( (queryResult) => { ... } ) 
.catch( (err) => { ... }); 
```

o con la notazione *async/await*

```node
try {
    let queryResult = Driver.update(
        "tableName",                // Nome della table
        [ 1 ],                      // Array con gli id da aggiornare
        [ "key1", "key2", ... ],    // array con i nomi delle chiavi (*dbSchema[i].keyName*).
        [ "value1", "value2", ... ] // Array coi nuovi valori da assegnare ai rispettivi campi.
    );
    ...
} catch (err) {
    ...
};
```
    
###  Test Cases

* **UPD01** - Risolve in `queryResult.success: true` ed aggiorna il record con parametri conformi.

[(Torna al Sommario)](#summary)

---

## <a name='delete'></a> `DbDriver.delete( tableName {String}, ids {Array} idKey {String} = "id" )`


Elimina uno o più record dalla `table` indicata come primo argomento.

###  Argomenti

* `tableName` {**String**}  Nome della tavola/schema su cui operare
* `ids` {**Array**}  Id dei records da eliminare.
* `idKey` {**String**} Nome del campo che funge da *id* del record, se diverso da `"id"` (valore di *default*).

###  Tipi Restituiti

`Promise(err, queryResult)`


L'oggetto `queryResult` contiene le seguenti proprietà:

* `success` **{Boolean}** `true` Se almeno un elemento è stato eliminato.
* `message` **{String}** Se `success=false` può contenenere il motivo dell'insuccesso.
* `query` **{String}** Stringa utilizzata per la query. 
* `parameters` **{Array}** Array di parametri utilizzati per la query.
* `affected` **{Number}** Numero di elementi eliminati.

### Test Cases

* **DEL01** - Risolve in `queryResult.success: true` ed elimina gli id indicati se corretti.
* **DEL02** - Risolve in `queryResult.success: true` ed elimina gli id corretti anche se presenti id errati nell'array fornito.
* **DEL03** - Risolve in `queryResult.success: false` e non elimina gli id forniti se tutti errati

[(Torna al Sommario)](#summary)

---


## <a name='stop'></a> `DbDriver.stop( force {Boolean} = false )`

Termina la connessione al DataBase in modo ordinato.

Metodo da chiamare quando si vuol terminare l'utilizzo del driver.

### Argomenti

* `force` {**Boolean**} Se fornito `true` arresta immediatamente il driver anche se altre istanze del driver sono in uso presso altri moduli. Se `false` (*default*) termina la connessione al driver solo se **tutte le istanze del Singleton hanno invocato tale metodo**.

### Tipi Restituiti

`Promise(err, Boolean)`

`true` se la connessione al DataBase (e il relativo thread) viene **effettivamente** terminata.

Per approfondimenti vedi il [paragrafo dedicato](#newstop).

### Test Cases

* **STP01** - Risolve in `false` e non arresta il driver se presenti altre copie dell'istanza che non hanno eseguito tale metodo.
* **STP02** - Risolve in `true` ed arresta immediatamente il driver se fornito argomento `force==true`

[(Torna al Sommario)](#summary)
