
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DEL MODULO DA TESTARE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
const tested = require("../")
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DEI MODULI NECESSARI ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
/** Utilità varie */
const jsutils           = require('leukos-tech-jsutils');

/** Funzione di test utilizzata in associazione a `mocha` */
const expect            = require('chai').expect;
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GESTIONE DEBUG ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
/** Proprietà di `process.env` in cui ricercare il valore indicante l'attivazione della modalità DEBUG (Verbose) dei test  */
const env_DEBUG_PROP    = process.env.DEBUG;

/** Se `true` i test vengono eseguiti loggando informazioni estese. Basata su `env_DEBUG_PROP`. */
const DEBUG_ENABLED     = (['1', 'true'].includes(env_DEBUG_PROP))

// console.warn(`DEBUG ENABLED: ${DEBUG_ENABLED}`)
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GESTIONE TIMING ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
/** Proprietà di `process.env` relativa al timing */
const env_TIMING_PROP   = process.env.TIMEIT;

/** Se `true' o 1 i l'esecuzione delle funzioni viene cronometrata */
const TIMEIT_ENABLED    = (['1', 1, 'true', true]).includes(env_TIMING_PROP);
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SHORT-ALIASES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

/**Formatta gli header per le intestazioni dei test (metodi `describe()`) */
const getHeader         = jsutils.tests.getHeader;
/** Formatta una stringa da loggare aggiungendo info come il nome della funzione */
const logStr            = jsutils.log.logString;
/** Handler risultati timing */
const TimingResults     = jsutils.tests.timeIt;
/** Short-Alias per `process.hrtime` */
const hrtime            = process.hrtime;
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DELLE RISORSE E MODULI ADDIZIONALI ~~~~~~~~~~~~~~~~~~~~~//
const params = require('./resources/poolParams');
const schemas = require('./resources/dbSchemas');

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//


// ============================================================================================ //
//                                DEFINIZIONE DEI TEST                                          //
// ============================================================================================ //

/** Header principale */
const mainHeader = 'MysqlDriver.ensureTable(tableName, schema) #interface #table-creation #mysql';

describe (`${getHeader(mainHeader, 0)}`, function() {
    this.timeout(0);
    // Variabili con Scope locale a questa unità vanno qui
    let driver=null, initDone=false, tableName="MyEnsuredTable";
    /** 
     * Memorizza l'istante iniziale del test.
     * 
     * Viene utilizzata per il TIMING SE ABILITATO */ 
    let start;

    // ~~~~~~~~~~~~~~~~ PER OGNI TEST CASE UN BLOCCO `it()`~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
    // Anteporre `async` se la funzione da testare è asyncrona (cb o promise)                                       //
    it(`ENS01 - Si risolve in \`queryResults.success: true\` dopo aver creato la table con parametri corretti`, async function(){

        let thisHeader = 'ENS01'
        let header = `${mainHeader} - ${thisHeader}`;

        // Skip del test in base ad alcune condizioni
        if (!driver) {
            /** Motivo dello skip */
            let reason = 'Istanza del driver non ottenuta correttamente';
            console.warn(`\t# TEST SKIPPED: ${reason} #`);
            this.skip();
        } 
        
        if (!initDone) {
            /** Motivo dello skip */
            let reason = 'Fallita inizializzazione del driver';
            console.warn(`\t# TEST SKIPPED: ${reason} #`);
            this.skip();
        } 
        
        /** Argomenti da utilizzare per la chiamata alla funzione */
        let arguments = [tableName, schemas.userSch];
        
        if (TIMEIT_ENABLED) start = hrtime();
        let res = await driver.ensureTable(...arguments);
        if (TIMEIT_ENABLED) timeRes.add(thisHeader, hrtime(start), arguments)
        
        if (DEBUG_ENABLED) console.debug(logStr(`${header} - Res: ${res}`));
        
        expect(res.success).equal(true);
        
    }); // ~~~~~~~~~~~~~ FINE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

    // ~~~~~~~~~~~~~~~~ PER OGNI TEST CASE UN BLOCCO `it()`~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
    // Anteporre `async` se la funzione da testare è asyncrona (cb o promise)                                       //
    it(`ENS02 - Si risolve in \`queryResults.success: true\` anche se eseguito per una table già esistente`, async function(){

        let thisHeader = 'ENS02'
        let header = `${mainHeader} - ${thisHeader}`;

        // Skip del test in base ad alcune condizioni
        if (!driver) {
            /** Motivo dello skip */
            let reason = 'Istanza del driver non ottenuta correttamente';
            console.warn(`\t# TEST SKIPPED: ${reason} #`);
            this.skip();
        } 
        
        if (!initDone) {
            /** Motivo dello skip */
            let reason = 'Fallita inizializzazione del driver';
            console.warn(`\t# TEST SKIPPED: ${reason} #`);
            this.skip();
        } 
        
        /** Argomenti da utilizzare per la chiamata alla funzione */
        let arguments = [tableName, schemas.userSch];
        
        if (TIMEIT_ENABLED) start = hrtime();
        let res = await driver.ensureTable(...arguments);
        if (TIMEIT_ENABLED) timeRes.add(thisHeader+" Creazione (chiamata #1)", hrtime(start), arguments)
        
        if (DEBUG_ENABLED) console.debug(logStr(`${header} - Res chiamata #1: ${res}`));
        
        expect(res.success).equal(true);
        
        if (TIMEIT_ENABLED) start = hrtime();
        res = await driver.ensureTable(...arguments);
        if (TIMEIT_ENABLED) timeRes.add(thisHeader+" Ripetizione (chiamata #2)", hrtime(start), arguments)
        
        if (DEBUG_ENABLED) console.debug(logStr(`${header} - Res chiamata #2: ${res}`));
        
        expect(res.success).equal(true);
        
    }); // ~~~~~~~~~~~~~ FINE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

    // ~~~~~~~~~~~~~~~~ PER OGNI TEST CASE UN BLOCCO `it()`~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
    // Anteporre `async` se la funzione da testare è asyncrona (cb o promise)                                       //
    it(`ENS03 - Si risolve in \`queryResults.success: false\` e non crea la table se \`schema\` non possiede la giusta formattazione`, async function(){

        let thisHeader = 'ENS03'
        let header = `${mainHeader} - ${thisHeader}`;

        // Skip del test in base ad alcune condizioni
        if (!driver) {
            /** Motivo dello skip */
            let reason = 'Istanza del driver non ottenuta correttamente';
            console.warn(`\t# TEST SKIPPED: ${reason} #`);
            this.skip();
        } 
        
        if (!initDone) {
            /** Motivo dello skip */
            let reason = 'Fallita inizializzazione del driver';
            console.warn(`\t# TEST SKIPPED: ${reason} #`);
            this.skip();
        } 
        
        for (let i=0; i<4; i++) {
            let wrongSchema = jsutils.io.copyObj(schemas.userSch);
            // console.log(`ITERAZIONE #${i}`)
            switch(i) {
                case 0:
                    wrongSchema[1].keyName = undefined;
                    break;
                case 1:
                    wrongSchema[1].type = undefined;
                    break;
                case 2:
                    wrongSchema[2].type = true
                    break;
                case 3:
                    wrongSchema[2].isReference = true;
                    break;
                
            }
            

            /** Argomenti da utilizzare per la chiamata alla funzione */
            let arguments = [tableName, wrongSchema];
            
            if (TIMEIT_ENABLED) start = hrtime();
            let res = await driver.ensureTable(...arguments);
            if (TIMEIT_ENABLED) timeRes.add(thisHeader+` iterazione #${i}`, hrtime(start), arguments)
            
            if (DEBUG_ENABLED) console.debug(logStr(`${header} - Res iterazione #${i}: ${res}`));
            
            expect(res.success).equal(false);

            
        
        }

        // Crea una table corretta per l'eliminazione in `afterEach`
        await driver.ensureTable(tableName, schemas.userSch);

        
        
    }); // ~~~~~~~~~~~~~ FINE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //



// ============================================================================================ //
//                                IMPOSTAZIONI DEI TEST                                         //
// ============================================================================================ //

    /** Metodo eseguito **prima del'esecuzione di tutta la suite** @async */
    before( async function () {
        if (TIMEIT_ENABLED) {timeRes = new TimingResults(mainHeader);}

        // Copia indipendente dei parametri, modificata, per impedire interferenze con gli altri test (non condivide il Singleton, quindi la pool rimane attiva)
        let pars = jsutils.io.copyObj(params.goodParams);
        pars.connectionLimit = 87;

        driver = new tested.driverFactory('mysql', DEBUG_ENABLED, console, 0);
        initDone = await driver.init(pars)
        

    });

    // /** Metodo eseguito **prima del'esecuzione di tutta la suite** */
    // before( function () {
    //     if (TIMEIT_ENABLED) {timeRes = new TimingResults(mainHeader);}
    // });

    /** Metodo eseguito **dopo l'esecuzione di tutta la suite** @async */
    after( async function () {
        if (TIMEIT_ENABLED) {console.info(timeRes.getResults())};
        
        // Arresta definitivamente questa istanza della pool
        await driver.stop();
    });

    /** Metodo eseguito **dopo l'esecuzione di tutta la suite** */
    // after( function () {
    // });

    /** Metodo eseguito **prima del'esecuzione di ogni test nella suite** @async */
    // beforeEach( async function () {
    // });

    /** Metodo eseguito **prima del'esecuzione di  ogni test nella suite** */
    // beforeEach( function () {
    // });

    /** Metodo eseguito **dopo l'esecuzione di  ogni test nella suite** @async */
    afterEach( async function () {
        // Elimina la table utilizzata per il test appena concluso
        await driver._pool.query(`DROP TABLE ${tableName};`);
        
    });

    /** Metodo eseguito **dopo l'esecuzione di  ogni test nella suite** */
    // afterEach( function () {
    // });

}); /** ---------------- fine della suite #1 ----------------------------------------*/