
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DEL MODULO DA TESTARE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
const tested = require("../")
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DEI MODULI NECESSARI ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
/** Utilità varie */
const jsutils           = require('leukos-tech-jsutils');

/** Funzione di test utilizzata in associazione a `mocha` */
const expect            = require('chai').expect;
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GESTIONE DEBUG ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
/** Proprietà di `process.env` in cui ricercare il valore indicante l'attivazione della modalità DEBUG (Verbose) dei test  */
const env_DEBUG_PROP    = process.env.DEBUG;

/** Se `true` i test vengono eseguiti loggando informazioni estese. Basata su `env_DEBUG_PROP`. */
const DEBUG_ENABLED     = (['1', 'true'].includes(env_DEBUG_PROP))

// console.warn(`DEBUG ENABLED: ${DEBUG_ENABLED}`)
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GESTIONE TIMING ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
/** Proprietà di `process.env` relativa al timing */
const env_TIMING_PROP   = process.env.TIMEIT;

/** Se `true' o 1 i l'esecuzione delle funzioni viene cronometrata */
const TIMEIT_ENABLED    = (['1', 1, 'true', true]).includes(env_TIMING_PROP);
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SHORT-ALIASES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

/**Formatta gli header per le intestazioni dei test (metodi `describe()`) */
const getHeader         = jsutils.tests.getHeader;
/** Formatta una stringa da loggare aggiungendo info come il nome della funzione */
const logStr            = jsutils.log.logString;
/** Handler risultati timing */
const TimingResults     = jsutils.tests.timeIt;
/** Short-Alias per `process.hrtime` */
const hrtime            = process.hrtime;

const copyObj           = jsutils.io.copyObj;
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DELLE RISORSE E MODULI ADDIZIONALI ~~~~~~~~~~~~~~~~~~~~~//
const params = require('./resources/poolParams');
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//


// ============================================================================================ //
//                                DEFINIZIONE DEI TEST                                          //
// ============================================================================================ //

/** Header principale */
const mainHeader = 'MysqlDriver.stop(force=false) #interface #stop #mysql';

describe (`${getHeader(mainHeader, 0)}`, function() {

    this.timeout(0);
    
    /** Memorizza l'istante iniziale del test (SE TIMING ATTIVO). */ 
    let start;
    
    // Altre variabili con Scope locale a questa unità vanno qui
    
    let driver;

    // ~~~~~~~~~~~~~~~~~~~~~~~ INIZIO DEFINIZIONE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
    it(`STP01 - Risolve in \`false\` e non arresta il driver se presenti altre copie dell'istanza che non hanno eseguito tale metodo.`, async function(){

        let thisHeader = 'STP01'
        let header = `${mainHeader} - ${thisHeader}`;

        // Skip del test in base ad alcune condizioni
        // if ('<someVariable>') {
        //     /** Motivo dello skip */
        //     let reason = '';
        //     console.warn(`\t# TEST SKIPPED: ${reason} #`);
        //     this.skip();
        // } 
        
        /** Argomenti da utilizzare per la chiamata alla funzione */
        let par = jsutils.io.copyObj(params.goodParams);
        par.connectionLimit = 907;
        let arguments = [par];
        
        let driver1 = tested.driverFactory(`mysql`, DEBUG_ENABLED);
        
        let initDone1 = await driver1.init(...arguments);
        
        let driver2 = tested.driverFactory(`mysql`, DEBUG_ENABLED);
        let initDone2 = await driver2.init(...arguments);

        expect(driver1._pool.__copies).equal(1);

        if (TIMEIT_ENABLED) start = hrtime();
        let res1 = await driver1.stop()
        if (TIMEIT_ENABLED) timeRes.add(thisHeader+" (Stop della copia)", hrtime(start), arguments);
            
        if (DEBUG_ENABLED) console.debug(logStr(`${header} - Res: ${res1}`));
        
        expect(res1).equal(false);
        expect(driver1._pool.__copies).equal(0);
        if (TIMEIT_ENABLED) start = hrtime();
        let res2 = await driver2.stop()
        if (TIMEIT_ENABLED) timeRes.add(thisHeader+" (Stop del Singleton)", hrtime(start), arguments);
        expect(res2).equal(true);

        
        
    }); // ~~~~~~~~~~~~~~~~~~~ FINE DEFINIZIONE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

    // ~~~~~~~~~~~~~~~~~~~~~~~ INIZIO DEFINIZIONE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
    it('STP02 - Risolve in `true` ed arresta immediatamente il driver se fornito argomento `force==true`', async function(){

        let thisHeader = 'STP02'
        let header = `${mainHeader} - ${thisHeader}`;

        // Skip del test in base ad alcune condizioni
        // if ('<someVariable>') {
        //     /** Motivo dello skip */
        //     let reason = '';
        //     console.warn(`\t# TEST SKIPPED: ${reason} #`);
        //     this.skip();
        // } 
        
        /** Argomenti da utilizzare per la chiamata alla funzione */
        let par = jsutils.io.copyObj(params.goodParams);
        par.connectionLimit = 909;
        let arguments = [par];
        
        let driver1 = tested.driverFactory(`mysql`, DEBUG_ENABLED);
        let initDone1 = await driver1.init(...arguments);
        let driver2 = tested.driverFactory(`mysql`, DEBUG_ENABLED);
        let initDone2 = await driver2.init(...arguments);
        
        expect(initDone2).equal(true);
        expect(initDone1).equal(true);
        
        expect(driver1._pool.__copies).equal(1);

        if (TIMEIT_ENABLED) start = hrtime();
        let res1 = await driver1.stop(true)
        if (TIMEIT_ENABLED) timeRes.add(thisHeader, hrtime(start), arguments);
            
        if (DEBUG_ENABLED) console.debug(logStr(`${header} - Res: ${res1}`));
        
        expect(res1).equal(true);
        let res2 = await driver2.stop();
        expect(res2).equal(false)
        try {
            let res = await driver2._pool.query('SHOW TABLES')
        } catch (err) {
            // console.warn(`Errore ${thisHeader}: ${err.name} - ${err.message}`)
            expect(err.name).equal('Error')
            expect(err.message).equal('Pool is closed.')
        }
        // expect(driver2._pool).equal(null)
        
        

        
        
    }); // ~~~~~~~~~~~~~~~~~~~ FINE DEFINIZIONE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //


// ============================================================================================ //
//                                IMPOSTAZIONI DEI TEST                                         //
//                   (rimuovere `async` per testase FUNZIONI SINCRONE)                          //
// ============================================================================================ //

    /** Metodo eseguito **prima del'esecuzione di tutta la suite** @async */
    before( async function () {
        if (TIMEIT_ENABLED) {timeRes = new TimingResults(mainHeader);}
        // driver = new tested.driverFactory('mysql', DEBUG_ENABLED)
    });

    /** Metodo eseguito **dopo l'esecuzione di tutta la suite** @async */
    after( async function () {
        if (TIMEIT_ENABLED) {console.info(timeRes.getResults())}
    });

    /** Metodo eseguito **prima del'esecuzione di ogni test nella suite** @async */
    // beforeEach( async function () {
    // });

    /** Metodo eseguito **dopo l'esecuzione di  ogni test nella suite** @async */
    // afterEach( async function () {
    // });


}); /** ---------------- fine della suite #1 ----------------------------------------*/