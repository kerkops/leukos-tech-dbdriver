"use strict";
// console.log(`DEBUG mysqlSingletonPool: ${process.env.DEBUG}`)

const DEBUG_ENABLED = (['1', 'true'].includes(process.env.DEBUG)) ? true : false
// console.log(`DEBUG_ENABLED mySqlSingletonPool: ${DEBUG_ENABLED} - ${typeof DEBUG_ENABLED}`);



const expect = require('chai').expect;
const tested = require('../lib/mysqlSingletonPool');
const par = require('./resources/poolParams')
const format = require('./resources/formatFuncs');

describe(format.getHeader('TEST SU mysqlSingletonPool.get() (Generatore del Singleton per la pool al database MySql)', 0), function(){

    this.timeout(0);
    
    let mainHeader = 'TEST SU mysqlSingletonPool.get()'
    let pool;
    
    it(`MSP01 - RESTITUISCE sempre un Singleton dell\'oggetto \`pool\` valido con parametri conformi ed invariati (SEMPRE LO STESSO OGGETTO)`, async function() {
        let header = `${mainHeader} - MSP01 - RESTITUISCE SEMPRE SINGLETON CON PARAMETRI INVARIATI`;
        try {
            
            pool = await tested.get(par.goodParams, DEBUG_ENABLED, console);
            
            expect([undefined, null].includes(pool)).equal(false);

            let pool2 = await tested.get(par.goodParams, DEBUG_ENABLED, console);

            expect([undefined, null].includes(pool2)).equal(false);

            expect(pool.poolName).equal(pool2.poolName);


        } catch(err) {
            err.message = `${header} - ERRORE - ${err.name} - ${err.message}`;
            throw(err);
        }
    });


    it('MSP02 - RESTITUISCE Singleton tra loro interconnessi dell\'oggetto \`pool\` (SE UNO VIENE CHIUSO LO SONO ANCHE GLI ALTRI)', async function() {
        let header = `${mainHeader} - MSP02 - RESTITUISCE SINGLETON INTERCONNESSI`;
        try {
            let errCount = 0;
            let pool1 = await tested.get(par.goodParams, DEBUG_ENABLED, console);
            let pool2 = await tested.get(par.goodParams, DEBUG_ENABLED, console);
            let pool3 = await tested.get(par.goodParams, DEBUG_ENABLED, console);
            
            expect(pool1).equal(pool2);
            expect(pool1).equal(pool3);
            
            /** ARRESTA LA POOL #1 - SE LE ALTRE NON SI ARRESTANO IL TEST FALLISCE */
            await pool1.end();
            
            try {
                let res2 = await pool2.query('SHOW DATABASES');
                this.fail(`${header} - TEST FALLITO - La pool ${pool2.poolName} è ANCORA ATTIVA`);
            } catch (err) {
                /** LA QUERY DEVE PRODURRE ERRORI (FERMATA) */
                errCount++;
            }
                
            try {
                let res3 = await pool3.query('SHOW DATABASES')    
            } catch (err) {
                /** LA QUERY DEVE PRODURRE ERRORI */
                errCount++;
            }
            
            // console.log(res)
            expect(errCount).equal(2);

        } catch (err) {
            err.message = `${header} - ERRORE - ${err.name} - ${err.message}`;
            if (DEBUG_ENABLED) console.error(err.message);
            throw(err);
        }
    });

    it('MSP03 - RESTITUISCE istanze istanze diverse ed indipendenti con parametri differenti (SE UNA ISTANZA CHIUSA LE ALTRE - DIVERSE - CONTINUANO NORMALMENTE)', async function() {
        let header = `${mainHeader} - MSP03 - ISTANZE MULTIPLE CON PARAMETRI DIFFERENTI`
        try {
            let pool1 = await tested.get(par.goodParams, DEBUG_ENABLED, console);
            let pool2 = await tested.get(par.goodParams2, DEBUG_ENABLED, console);
            
            expect(pool1).not.equal(pool2);
            expect(pool1.poolName).not.equal(pool2.poolName);
            
            await pool1.end();

            let res2 = await pool2.query('SHOW DATABASES');

            expect(res2.length).not.equal(0);
            
            // console.log(res2)
            await pool2.end();
            
            

        } catch (err) {
            err.message = `${header} - ERRORE - ${err.name} - ${err.message}`;
            if (DEBUG_ENABLED) console.error(err.message);
            throw(err);
        }
    })
    
    afterEach( async function() {
        try {
            await pool.end();
        } catch (err) {
            console.error(`mySingletonPool.test.js - ERRORE CHIUSURA POOL: ${err.name} - ${err.message}`);
        }
    })
});