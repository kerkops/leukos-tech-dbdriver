const TEST_TABLE_NAME = "users_test_table";
const DEBUG_ENABLED = (['1', 'true'].includes(process.env.DEBUG)) ? true : false
const expect = require('chai').expect;
const Driver = require('../lib/mysqlDriver');

const params = require('./resources/poolParams');
const schemas = require('./resources/dbSchemas');
const entries = require('./resources/dbEntries');
const format = require ('./resources/formatFuncs');
let driver;// = new Driver (DEBUG_ENABLED);

describe (`${format.getHeader('MysqlDriver._Tb_getSchema(tableName)', 0)}`, function() {
  this.timeout(0);

  let INIT_DONE = false;
  let mainHeader = `TEST SU mysqlDriver._Tb_getSchema(tableName)`;
  let insertedIds = [];
  let tableExists = false;
  before( async function () {
    try {
      driver = new Driver(DEBUG_ENABLED, console);
      INIT_DONE = await driver.init(params.goodParams);
      // INIT_DONE = false;
      // console.log(driver)
      tableExists = await driver.ensureTable(TEST_TABLE_NAME, schemas.userSch);
      await entries.clearTable(driver, TEST_TABLE_NAME);
      insertedIds = await entries.populateTable(driver, TEST_TABLE_NAME, entries.users1);
      if (DEBUG_ENABLED) console.warn(`${mainHeader} - Before() - InsertedIds: ${insertedIds}`)
      // tableExists = false;
      // insertedIds = [];
      // insertedIds = undefined;
      // done();

    } catch (err) {
      err.message = `${mainHeader} - Before() fallita - ${err.name} - ${err.message}`;
      if (DEBUG_ENABLED) console.warn(err.message);
      // throw(err);
    }
      
  });

  after (async function(){
    try {
      if(tableExists && insertedIds && insertedIds.length) {
        if (DEBUG_ENABLED) console.warn (`${mainHeader} - Table ${TEST_TABLE_NAME} in eliminazione...`)
        await entries.clearTable(driver, TEST_TABLE_NAME)
      }
      let stopped = await driver.stop();

      
    } catch (err) {
      err.message = `${mainHeader} - After() fallita - ${err.name} - ${err.message}`;
      console.warn(err.message);
      // throw(err);
    }
  });

  it (`Restituisce l'Array delle chiavi per una table esistente`, async function(){
    let header = `${mainHeader} - Verifica che restituisca l'array corretto`;

    if (!INIT_DONE) {
        console.warn(`\t# TEST SKIPPED: Driver.init(params) ha restituito 'false' #`);
        this.skip();
    } else if (!tableExists) {
        console.warn(`\t# TEST SKIPPED: Driver.ensureTable(tableName) ha restituito 'false' #`);
        this.skip();
    };

    try {
        let res = await driver._Tb_getSchema(TEST_TABLE_NAME);
        if (DEBUG_ENABLED) console.warn(`${header} - ResponseObject: ${JSON.stringify(res)}`);
        expect(res.success).equal(true);
        expect(res.affected).equal(schemas.userSch.length);
        
    } catch (err) {
        err.message = `${header} - ERRORE - ${err.name}: ${err.message}`;
        if(DEBUG_ENABLED) console.error(err.message);
        // throw(err);

        
    }
  })

});