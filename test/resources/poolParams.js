const goodParams = {
    connectionLimit: 10,
    host: 'localHost',
    user: 'alfred_admin',
    password: 'L1l1th1sL0v3!',
    database: 'AlfredTest'
};

const goodParams2 = {
    connectionLimit: 100,
    host: 'localHost',
    user: 'alfred_admin',
    password: 'L1l1th1sL0v3!',
    database: 'AlfredTest'
};



const badParamsPWD = {
    connectionLimit: 1,
    host: 'localHost',
    user: 'alfred_admin',
    password: 'error',
    database: 'AlfredTest'
};

const badParamsHOST = {
    connectionLimit: 1,
    host: 'wrongHost',
    user: 'alfred_admin',
    password: 'L1l1th1sL0v3',
    database: 'AlfredTest'
};

const badParamsUSER = {
    connectionLimit: 1,
    host: 'localHost',
    user: 'wrongUser',
    password: 'L1l1th1sL0v3',
    database: 'AlfredTest'
};

const badParamsNO_DB = {
    connectionLimit: 1,
    host: 'localHost',
    user: 'alfred_admin',
    password: 'L1l1th1sL0v3',
    // database: undefined
};

const badParamsNO_USR = {
    connectionLimit: 10,
    host: 'localHost',
    // user: 'alfred_admin',
    password: 'L1l1th1sL0v3!',
    database: 'AlfredTest'
};

const badParamsNO_PWD = {
    connectionLimit: 10,
    host: 'localHost',
    user: 'alfred_admin',
    // password: 'L1l1th1sL0v3!',
    database: 'AlfredTest'
};

module.exports = {
    goodParams: goodParams,
    goodParams2: goodParams2,
    badParamsHOST: badParamsHOST,
    badParamsPWD: badParamsPWD,
    badParamsUSER: badParamsUSER,
    badParamsNO_DB: badParamsNO_DB,
    badParamsNO_PWD: badParamsNO_PWD,
    badParamsNO_USR: badParamsNO_USR
}