const params = require('./poolParams')

const DEBUG_ENABLED = (['1', 'true'].includes(process.env.DEBUG)) ? true : false

const users1 = {
    keys : [ 'username', 'email', 'password', 'userpic', 'created' ],
    records: [
        ['username_luca', 'email_luca', 'password_luca', null, new Date().toISOString().slice(0, 19).replace('T', ' ')],
        ['username_sofia', 'email_sofia', 'password_sofia', null, new Date().toISOString().slice(0, 19).replace('T', ' ')],
        ['username_mario', 'email_mario', 'password_mario', null, new Date().toISOString().slice(0, 19).replace('T', ' ')],
        ['username_luigi', 'email_luigi', 'password_luigi', null, new Date().toISOString().slice(0, 19).replace('T', ' ')],
        ['username_lilith', 'email_lilith', 'password_lilith', null, new Date().toISOString().slice(0, 19).replace('T', ' ')],
        ['username_marta', 'email_marta', 'password_marta', null, new Date().toISOString().slice(0, 19).replace('T', ' ')],
        ['username_zangief', 'email_zangief', 'password_zangief', null, new Date().toISOString().slice(0, 19).replace('T', ' ')],
        ['username_chun-li', 'email_chun-li', 'password_chun-li', null, new Date().toISOString().slice(0, 19).replace('T', ' ')]
        
    ] 
};

const companies1 = {
    keys : ['name'],
    records : [['civico4'], ['blind pig'], ['mc donalds'], ['macrosolution']]
};

const userComp1 = {
    keys : ["company_id", "user_id"]
}

/**
 * Funzione di convenienza per popolare le table in caso di unittesting.
 * 
 * **UTILIZZA IL METODO .insert() DEL DRIVER**
 * 
 * @param {Object} driver Istanze di MySqlriver da testare
 * @param {String} tableName Nome della table da riempire dei campi 
 * @param {Array} recordsObject Oggetto contenente le proprietà *keys* (Array con le chiavi della table) e *records* (Array di Array dei valori da inserire).
 * 
 * @return {Array} Array con gli id dei record inseriti con successo.
 */
async function populateTable (driver, tableName, recordsObject) {
    let insIds = []
    let header = `populateTable(${tableName}, ${recordsObject})`;
    // if (DEBUG_ENABLED) console.log(header);

    for (let record of recordsObject.records){
        try{    
            // if (DEBUG_ENABLED) console.warn('## POPOLO LA TABLE '+tableName);
            let inserted = await driver.insert(tableName, recordsObject.keys, record);

            if (!inserted) {
                throw new TypeError(`L'inserimento di ${JSON.stringify(record)} nella table "${tableName}"è fallito. ${inserted.message} (${inserted.query} - ${inserted.params})`);
            };
            // if (DEBUG_ENABLED) console.log(`${header} - QueryResult: ${JSON.stringify(inserted)}`);
            insIds.push(...inserted.insertedIds);
            // if (DEBUG_ENABLED) console.log(`${header} - Inserted: ${insIds}`);
        } catch (err) {
            if (DEBUG_ENABLED) console.error(`populateTable() ERRORE NELL'ISERIMENTO DI DATI FITTIZI - ${err.name}: "${err.message}"`);
            return [];
        };
    };
    // if (DEBUG_ENABLED) console.log(`${header} - Returning: ${insIds}`);
    return insIds;
};

/**
 * Azzera il contenuto di una table fornita come argomento tramite comando diretto.
 * 
 * **UTILIZZA IL METODO *_pool.query()* DEL DRIVER, MA NESSUN METODO DALL'INTERFACCIA DEL DRIVER**
 * 
 * @param {Object} driver Istanza di MySqlDriver da testare
 * @param {String} tableName Nome della table da resettare.
 * 
 * @async 
 */
async function clearTable (driver, tableName) {
    try {
        let sql = "TRUNCATE TABLE "+tableName+";";
        let res = await driver._pool.query(sql);
        if (DEBUG_ENABLED) console.log(`clearTable() - RAW RESPONSE: ${JSON.stringify(res)}`)
        
    } catch (err) {
        if (DEBUG_ENABLED) console.error(`clearTable() - ERRORE NELL'AZZERAMENTO DELLA TABLE FITTIZIA - ${err.name}: "${err.message}"`);
        // throw(err);
        
    }

};

/** 
 * Esegue l'eliminazione della table tramite comando diretto (NON USA IL DRIVER) 
 * 
 * **UTILIZZA IL METODO *_pool.query()* DEL DRIVER, MA NESSUN METODO DALL'INTERFACCIA DEL DRIVER**
 * 
 * @param {object} driver Istanza mysql driver col metodo async  '_pool.query()'
 * @param {string} tableName Nome della table da eliminare 
 * 
 * @async Promise (err, <boolean>)
 * */
async function manuallyDropTable(driver, tableName) {
    // let cmd = `DROP TABLE IF EXISTS \`${tableName}\`;`
    let cmd = `DROP TABLE IF EXISTS \`${params.goodParams.database}\`.\`${tableName}\`;`
    try {
      let res = await driver._pool.query(cmd)
      if (res.constructor.name == 'OkPacket') {
        if (DEBUG_ENABLED) console.debug(`manuallyDropTable() - TABLE "${tableName}" ELIMINATA con successo`)
        return true
      }
    } catch (err) {
      err.message = `manuallyDropTable() - IMPOSSIBILE ESEGUIRE CMD "${cmd}" - ${err.name} - ${err.message}`;
  
      if (DEBUG_ENABLED) console.error(err.message);
  
      
    }
    return false;
  
  
};

/** 
 * Restituisce il risultato della query manuale DESCRIBE TABLE <TEST_TABLE_NAME>
 * 
 * **UTILIZZA IL METODO *_pool.query()* DEL DRIVER, MA NESSUN METODO DALL'INTERFACCIA DEL DRIVER**
 * 
 * @param {object} driver Istanza mysql driver col metodo async  '_pool.query()'.
 * @param {string} tableName Nome della table di cui si vuol conoscere la struttura
 * 
 * @async Promise (err, QueryPacket(s))
 */
async function manuallyDescribeTable(driver, tableName = TEST_TABLE_NAME) {
  
    let header = `manuallyDescribeTable("${tableName}")`;
    // let query = `DESCRIBE \`${tableName}\`;`;
    let query = `DESCRIBE \`${params.goodParams.database}\`.\`${tableName}\`;`;
    try {
    // await driver._pool.query(`USE ${params.goodParams.database};`)
    let res = await driver._pool.query(query);
    return res
      
    } catch (err) {
      err.message = `${header} - ERRORE NELLA QUERY "${query}" - ${err.name} - ${err.message}`;
      if (DEBUG_ENABLED) console.error(err.message);
      // throw err;
    }
  
  };
  



module.exports = {
    users1:users1,
    companies1:companies1,
    userComp1: userComp1,
    populateTable: populateTable,
    clearTable: clearTable,
    dropTable: manuallyDropTable,
    describeTable: manuallyDescribeTable
};
