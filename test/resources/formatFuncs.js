const DEBUG_ENABLED = (['1', 'true'].includes(process.env.DEBUG)) ? true : false


const HEADER_SPACE = 150;
const PADDING = 10;
const DELTA_MULTIPLIER = 2;


function getHeaderString(headerString, delimiter, spacer, delta=0, extra='') {
  let verSpacing = DEBUG_ENABLED ? '\n' : '';
  let specerLen = (HEADER_SPACE - headerString.length ) - PADDING;
  return `\n  ${verSpacing}${delimiter}${spacer.repeat(PADDING-(delta*DELTA_MULTIPLIER))} ${headerString} ${spacer.repeat(specerLen-(delta*DELTA_MULTIPLIER))}${delimiter}${extra}${verSpacing}`;
};
function formatHeader(header, level=2) {
  switch (level) {
    case 0:
      return getHeaderString(header, '|', "=", level, "\n\n");
    case 1:
      return getHeaderString(header, ':', '-', level, "\n");
    default:
      return getHeaderString(header, '-', '-', level)
  }
};


module.exports = {
  getHeader: formatHeader,
  getHeaderString: getHeaderString
}