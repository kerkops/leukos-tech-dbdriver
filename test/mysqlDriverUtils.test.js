const expect = require('chai').expect;
const sqlUtils = require('../lib/mysqlDriverUtils');
const format = require('./resources/formatFuncs');
// const DEBUG_ENABLED = process.env.DEBUG;

const dbSch = require('./resources/dbSchemas');

describe(format.getHeader("TEST SU mysqlDriverUtils.js #driver-utilities", 0), function() {
    this.timeout(0);
    
    describe(format.getHeader('TEST SU getDataType(type, size)', 1), function(){
        it('GDT01 - SOLLEVA \`TypeError\` se \`type\` ha un valore non supportato', function(){
            expect(function() {sqlUtils.getDataType('wrong'); }).to.throw(TypeError)
        });

        describe(format.getHeader('\`type\` == "char"', 2), function(){
             
            it('GDC02 - SOLLEVA \`RangeError\` con \`size\` == 0', function(){
                expect(function() {sqlUtils.getDataType('char', 0); }).to.throw(RangeError);
            });

            it('GDC03 - RESTITUISCE "CHAR(size)" con 1 <= \`size\` <= 255 ', function(){
                for (let i=1; i <256; i++) {
                    let res = sqlUtils.getDataType('char', i);
                    expect(res).equal(`CHAR(${i})`);
                };
            });
        });

        describe(format.getHeader('\`type\` == "string"', 2), function(){
        
            it(`GDS01 - RESTITUISCE "VARCHAR(size)" con 0 < \`size\` <= ${sqlUtils.MAX_VARCHAR_SIZE} (\`MAX_VARCHAR_SIZE\`)`, function(){
                for (let i=1; i < sqlUtils.MAX_VARCHAR_SIZE; i+=100) {
                    let res = sqlUtils.getDataType('String', i);
                    expect(res).equal(`VARCHAR(${i})`);
                };
            });

            it('GDS02 - RESTITUISCE "VARCHAR(255)" con \`size\` NON FORNITO', function(){
                let res = sqlUtils.getDataType('string');
                expect(res).equal('VARCHAR(255)');

            });

            it(`GDS03 - RESTITUISCE "TEXT" con \`size\` >= ${sqlUtils.MAX_VARCHAR_SIZE} (\`MAX_VARCHAR_SIZE\`)`, function(){
                let res = sqlUtils.getDataType('string', sqlUtils.MAX_VARCHAR_SIZE);
                expect(res).equal('TEXT');

            });

        });

        describe(format.getHeader('\`type\` == "number", "int", "integer", "intero"' , 2), function(){

            it('GDN01 - RESTITUISCE "TINYINT" con \`size\` == 1', function(){
                for (let _type of ['Number', 'iNt', 'INTEGER', 'IntEro']){
                    let res = sqlUtils.getDataType(_type, 1);
                    expect(res).equal('TINYINT')
                }    
            });
            it('GDN02 - RESTITUISCE "SMALLINT" con \`size\`== 2 o NON FORNITA', function(){
                for (let _type of ['Number', 'iNt', 'INTEGER', 'IntEro']){
                    let res = sqlUtils.getDataType(_type, 2);
                    expect(res).equal('SMALLINT');
                    let res2 = sqlUtils.getDataType(_type);
                    expect(res2).equal('SMALLINT');
                }    
            });
            it('GDN03 - RESTITUISCE "MEDIUMINT" con \`size\` == 3', function(){
                for (let _type of ['Number', 'iNt', 'INTEGER', 'IntEro']){
                    let res = sqlUtils.getDataType(_type, 3);
                    expect(res).equal('MEDIUMINT')
                }    
            });
            it('GDN04 - RESTITUISCE "INT" con \`size\` == 4', function(){
                for (let _type of ['Number', 'iNt', 'INTEGER', 'IntEro']){
                    let res = sqlUtils.getDataType(_type, 4);
                    expect(res).equal('INT')
                }    
            });

            it('GDN05 RESTITUISCE "BIGINT" con 5 <= \`size\` <= 8', function(){
                for (let _type of ['Number', 'iNt', 'INTEGER', 'IntEro']){
                    for (let _size of [5,6,7,8]) {
                        let res = sqlUtils.getDataType(_type, _size);
                        expect(res).equal('BIGINT')    
                    }
                    
                }    
        
            });

            it('GDN06 - SOLLEVA \`TypeError\` con \`size\` == 0', function(){
                expect(function() {sqlUtils.getDataType('number', 0); }).to.throw(TypeError);
            });

            it('GDN07 - SOLLEVA \`TypeError\` con \`size\` >= 9', function(){
                expect(function() {sqlUtils.getDataType('number', 9); }).to.throw(TypeError);
            });

            
        });

        describe(format.getHeader(`\`type\` == "float", "double"`, 2), function() {
            it('GDN08 - RESTITUISCE "FLOAT" con \'type\' == "float" e IGNORA ARGOMENTO \`size\`', function(){
                let res = sqlUtils.getDataType('floAt', 123);
                expect(res).equal('FLOAT');
            });
            it('GDN09 - RESTITUISCE "DOUBLE" con \'type\' == "double" e IGNORA ARGOMENTO \`size\`', function(){
                let res = sqlUtils.getDataType('DOUble', 123);
                expect(res).equal('DOUBLE');
            });



        });

        describe(format.getHeader('Altri DataType', 2), function(){
            it('GDO01 - RESTITUISCE "BOOLEAN" con \'type\' == "bool" o "boolean" e IGNORA ARGOMENTO \`size\`', function(){
                let res = sqlUtils.getDataType('Bool', 123);
                expect(res).equal('BOOLEAN');
                let res2 = sqlUtils.getDataType('BoolEan', 123);
                expect(res2).equal('BOOLEAN');
            });

            it('GDO02 - RESTITUISCE "LONGBLOB" con \`type\` == "blob", "image", "audio", "mediumblob", "longblob" e IGNORA ARGOMENTO \`size\`', function(){
                for (let _type of ['Blob', 'Image', 'aUdio', 'MediumBLOB', 'LONGBLOB']){
                    let res = sqlUtils.getDataType(_type, 1000);
                    expect(res).equal('LONGBLOB')
                }
            });

            it('GDO03 - RESTITUISCE "DATE" con \`type\` == "date" e IGNORA ARGOMENTO \`size\`', function(){
                let res = sqlUtils.getDataType('Date', 123);
                expect(res).equal('DATE');
            });
            
            it('GDO04 - RESTITUISCE "DATETIME" con \`type\` == "datetime" e IGNORA ARGOMENTO \`size\`', function(){
                let res = sqlUtils.getDataType('DateTime', 123);
                expect(res).equal('DATETIME');
            });
            
            it('GDO05 - RESTITUISCE "TIME" con \`type\` == "time" e IGNORA ARGOMENTO \`size\`', function(){
                let res = sqlUtils.getDataType('tIME', 123);
                expect(res).equal('TIME');
            });
            
            it('GDO06 - RESTITUISCE "TIMESTAMP"  con \`type\` == "timestamp" e IGNORA ARGOMENTO \`size\`', function(){
                let res = sqlUtils.getDataType('timeStamp', 123);
                expect(res).equal('TIMESTAMP');
            });
            


        });


        
    });

    // describe(format.getHeader('TEST SU getCreateTableCmd(tableName, schema)', 1), function() {
        
    //     it("GCT01 - SOLLEVA \`TypeError\` se uno degli oggetti nello schema non possiede le proprietà .keyname e .type", function() {

    //         let wrongSch = JSON.parse(JSON.stringify(dbSch.userSch));
    //         wrongSch.push({"Wrong:key":"value", "type":"String"});
            
    //         let wrongSchTP = JSON.parse(JSON.stringify(dbSch.userSch));
    //         wrongSchTP.push({"keyName": 'akey', "wrong:type":"string"})
            
    //         try {
    //             let res = sqlUtils.cmd.TB_create('user', wrongSch);

    //         } catch(err) {
    //             expect(err.name).equal('TypeError');
    //         };

    //         try {
    //             let res = sqlUtils.cmd.TB_create('user', wrongSchTP);

    //         } catch(err) {
    //             expect(err.name).equal('TypeError');
    //         };

    //     })
    // });

    
    // }) ;

});