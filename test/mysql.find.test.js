
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DEL MODULO DA TESTARE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
const tested = require("../")
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DEI MODULI NECESSARI ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
/** Utilità varie */
const jsutils           = require('leukos-tech-jsutils');

/** Funzione di test utilizzata in associazione a `mocha` */
const expect            = require('chai').expect;
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GESTIONE DEBUG ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
/** Proprietà di `process.env` in cui ricercare il valore indicante l'attivazione della modalità DEBUG (Verbose) dei test  */
const env_DEBUG_PROP    = process.env.DEBUG;

/** Se `true` i test vengono eseguiti loggando informazioni estese. Basata su `env_DEBUG_PROP`. */
const DEBUG_ENABLED     = (['1', 'true'].includes(env_DEBUG_PROP))

// console.warn(`DEBUG ENABLED: ${DEBUG_ENABLED}`)
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GESTIONE TIMING ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
/** Proprietà di `process.env` relativa al timing */
const env_TIMING_PROP   = process.env.TIMEIT;

/** Se `true' o 1 i l'esecuzione delle funzioni viene cronometrata */
const TIMEIT_ENABLED    = (['1', 1, 'true', true]).includes(env_TIMING_PROP);
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SHORT-ALIASES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

/**Formatta gli header per le intestazioni dei test (metodi `describe()`) */
const getHeader         = jsutils.tests.getHeader;
/** Formatta una stringa da loggare aggiungendo info come il nome della funzione */
const logStr            = jsutils.log.logString;
/** Handler risultati timing */
const TimingResults     = jsutils.tests.timeIt;
/** Short-Alias per `process.hrtime` */
const hrtime            = process.hrtime;
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DELLE RISORSE E MODULI ADDIZIONALI ~~~~~~~~~~~~~~~~~~~~~//
const params = require('./resources/poolParams');
const schemas = require('./resources/dbSchemas');
const entries = require('./resources/dbEntries');

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//


// ============================================================================================ //
//                                DEFINIZIONE DEI TEST                                          //
// ============================================================================================ //

/** Header principale */
const mainHeader = 'MysqlDriver.find(tableName, keys, values) #interface #data-find #mysql';

describe (`${getHeader(mainHeader, 0)}`, function() {

    this.timeout(0);
    
    /** Memorizza l'istante iniziale del test (SE TIMING ATTIVO). */ 
    let start;
    
    // Altre variabili con Scope locale a questa unità vanno qui
    let driver=null, initDone=false, insertedIds=null, tableExists=false;
    

    // ~~~~~~~~~~~~~~~~~~~~~~~ INIZIO DEFINIZIONE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
    it(`FIN01 - Risolve in 'queryResult.success:true' e 'queryResult.data' contiene i record attesi con valori conformi`, async function(){

        let thisHeader = 'FIN01'
        let header = `${mainHeader} - ${thisHeader}`;

        let skipMessage = shouldSkip();

        // Skip del test in base ad alcune condizioni
        if (skipMessage) {
            /** Motivo dello skip */
            // let reason = '';
            console.warn(`\t# TEST SKIPPED: ${skipMessage} #`);
            this.skip();
        } 
        
        /** Argomenti da utilizzare per la chiamata alla funzione */
        let arguments = [schemas.userTable, ["id"], [insertedIds[0]]];
        
        if (TIMEIT_ENABLED) start = hrtime();
        let res = await driver.find(...arguments);
        if (TIMEIT_ENABLED) timeRes.add(thisHeader, hrtime(start), arguments)
        
        if (DEBUG_ENABLED) console.debug(logStr(`${header} - Res: ${res}`));
        
        expect(res.success).equal(true);
        expect(res.affected).equal(1);
        expect(res.data[0].id).equal(insertedIds[0]);
        
    }); // ~~~~~~~~~~~~~~~~~~~ FINE DEFINIZIONE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

    // ~~~~~~~~~~~~~~~~~~~~~~~ INIZIO DEFINIZIONE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
    it(`FIN02 - Risolve in 'queryResult.success:true' e 'queryResult.data' contiene tutti i record della table se chiamato senza argomenti (eccetto 'tableName')`, async function(){

        let thisHeader = 'FIN02'
        let header = `${mainHeader} - ${thisHeader}`;

        let skipMessage = shouldSkip();

        // Skip del test in base ad alcune condizioni
        if (skipMessage) {
            /** Motivo dello skip */
            // let reason = '';
            console.warn(`\t# TEST SKIPPED: ${skipMessage} #`);
            this.skip();
        } 
        
        /** Argomenti da utilizzare per la chiamata alla funzione */
        let arguments = [schemas.userTable];
        
        if (TIMEIT_ENABLED) start = hrtime();
        let res = await driver.find(...arguments);
        if (TIMEIT_ENABLED) timeRes.add(thisHeader, hrtime(start), arguments)
        
        if (DEBUG_ENABLED) console.debug(logStr(`${header} - Res: ${res}`));
        
        expect(res.success).equal(true);
        expect(res.affected).equal(insertedIds.length);
        // expect(res.data[0].id).equal(insertedIds[0]);
        
    }); // ~~~~~~~~~~~~~~~~~~~ FINE DEFINIZIONE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

    // ~~~~~~~~~~~~~~~~~~~~~~~ INIZIO DEFINIZIONE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
    it(`FIN03 - Risolve in 'queryResult.success:false' se 'keys' e 'values' sono di lunghezza diversa`, async function(){

        let thisHeader = 'FIN03'
        let header = `${mainHeader} - ${thisHeader}`;

        let skipMessage = shouldSkip();

        // Skip del test in base ad alcune condizioni
        if (skipMessage) {
            /** Motivo dello skip */
            // let reason = '';
            console.warn(`\t# TEST SKIPPED: ${skipMessage} #`);
            this.skip();
        } 
        
        /** Argomenti da utilizzare per la chiamata alla funzione */
        let arguments = [schemas.userTable, ["id"], [insertedIds[0], "aValuesMore"] ];
        
        if (TIMEIT_ENABLED) start = hrtime();
        let res = await driver.find(...arguments);
        if (TIMEIT_ENABLED) timeRes.add(thisHeader, hrtime(start), arguments)
        
        if (DEBUG_ENABLED) console.debug(logStr(`${header} - Res: ${res}`));
        
        expect(res.success).equal(false);
        expect(res.affected).equal(0);
        // expect(res.data[0].id).equal(insertedIds[0]);
        
    }); // ~~~~~~~~~~~~~~~~~~~ FINE DEFINIZIONE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

    // ~~~~~~~~~~~~~~~~~~~~~~~ INIZIO DEFINIZIONE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
    it(`FIN04 - Risolve in 'queryResult.success:true' e 'queryResult.data' contiene i record attesi con valori conformi (campi multipli diversi da id)`, async function(){

        let thisHeader = 'FIN01'
        let header = `${mainHeader} - ${thisHeader}`;

        let skipMessage = shouldSkip();

        // Skip del test in base ad alcune condizioni
        if (skipMessage) {
            /** Motivo dello skip */
            // let reason = '';
            console.warn(`\t# TEST SKIPPED: ${skipMessage} #`);
            this.skip();
        } 
        let keys = [entries.users1.keys[1], entries.users1.keys[2]];
        let values = [entries.users1.records[2][1], entries.users1.records[2][2]];
        // console.warn(`Keys: ${keys} || Values: ${values}`)
        /** Argomenti da utilizzare per la chiamata alla funzione */
        let arguments = [schemas.userTable, keys, values];
        
        if (TIMEIT_ENABLED) start = hrtime();
        let res = await driver.find(...arguments);
        if (TIMEIT_ENABLED) timeRes.add(thisHeader, hrtime(start), arguments)
        
        if (DEBUG_ENABLED) console.debug(logStr(`${header} - Res: ${res}`));
        
        expect(res.success).equal(true);
        expect(res.affected).equal(1);
        expect(res.data[0].id).equal(insertedIds[2]);
        
    }); // ~~~~~~~~~~~~~~~~~~~ FINE DEFINIZIONE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

    function shouldSkip() {
        if (!driver) return "Istanza del driver non ottenuta correttamente"
        if (!initDone) return "Inizializzazione del driver fallita"
        if (!tableExists) return "Creazione table test fallita"
        if (!insertedIds || !insertedIds.length) return "Fallito inserimento record nella test table"
    }

// ============================================================================================ //
//                                IMPOSTAZIONI DEI TEST                                         //
//                   (rimuovere `async` per testase FUNZIONI SINCRONE)                          //
// ============================================================================================ //

    /** Metodo eseguito **prima del'esecuzione di tutta la suite** @async */
    before( async function () {
        if (TIMEIT_ENABLED) {timeRes = new TimingResults(mainHeader);}
        driver = tested.driverFactory('mysql', DEBUG_ENABLED, console, 0);
        let thisParams = jsutils.io.copyObj(params.goodParams2);
        thisParams.connectionLimit = 912;
        initDone = await driver.init(thisParams);
        if (initDone) {
            tableExists = await driver.ensureTable(schemas.userTable, schemas.userSch);
            if (tableExists) {
                insertedIds = await entries.populateTable(driver, schemas.userTable, entries.users1);
            };
        }
        
        

    });

    /** Metodo eseguito **dopo l'esecuzione di tutta la suite** @async */
    after( async function () {
        if (TIMEIT_ENABLED) {console.info(timeRes.getResults())};
        if (tableExists) {
            await entries.dropTable(driver, schemas.userTable);
        };
        await driver.stop(true);
    });

    /** Metodo eseguito **prima del'esecuzione di ogni test nella suite** @async */
    // beforeEach( async function () {
    // });

    /** Metodo eseguito **dopo l'esecuzione di  ogni test nella suite** @async */
    // afterEach( async function () {
    // });


}); /** ---------------- fine della suite #1 ----------------------------------------*/