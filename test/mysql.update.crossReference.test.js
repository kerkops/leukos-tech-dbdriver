
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DEL MODULO DA TESTARE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
const tested = require("../");
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DEI MODULI NECESSARI ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
/** Utilità varie */
const jsutils           = require('leukos-tech-jsutils');

/** Funzione di test utilizzata in associazione a `mocha` */
const expect            = require('chai').expect;
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GESTIONE DEBUG ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
/** Proprietà di `process.env` in cui ricercare il valore indicante l'attivazione della modalità DEBUG (Verbose) dei test  */
const env_DEBUG_PROP    = process.env.DEBUG;

/** Se `true` i test vengono eseguiti loggando informazioni estese. Basata su `env_DEBUG_PROP`. */
const DEBUG_ENABLED     = (['1', 'true'].includes(env_DEBUG_PROP))

// console.warn(`DEBUG ENABLED: ${DEBUG_ENABLED}`)
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GESTIONE TIMING ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
/** Proprietà di `process.env` relativa al timing */
const env_TIMING_PROP   = process.env.TIMEIT;

/** Se `true' o 1 i l'esecuzione delle funzioni viene cronometrata */
const TIMEIT_ENABLED    = (['1', 1, 'true', true]).includes(env_TIMING_PROP);
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SHORT-ALIASES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

/**Formatta gli header per le intestazioni dei test (metodi `describe()`) */
const getHeader         = jsutils.tests.getHeader;
/** Formatta una stringa da loggare aggiungendo info come il nome della funzione */
const logStr            = jsutils.log.logString;
/** Handler risultati timing */
const TimingResults     = jsutils.tests.timeIt;
/** Short-Alias per `process.hrtime` */
const hrtime            = process.hrtime;
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DELLE RISORSE E MODULI ADDIZIONALI ~~~~~~~~~~~~~~~~~~~~~//
const params = require('./resources/poolParams');
const schemas = require('./resources/dbSchemas');
const entries = require('./resources/dbEntries');
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//


// ============================================================================================ //
//                                DEFINIZIONE DEI TEST                                          //
// ============================================================================================ //

/** Header principale */
const mainHeader = 'MsqlDriver.update(tableName, ids, keys, values) CROSS #cross-referenced #interface #data-crud #data-update #mysql';

describe (`${getHeader(mainHeader, 0)}`, function() {

    this.timeout(0);
    
    // Variabili con Scope locale a questa unità vanno qui
    let driver = null,
        initDone = false,
        tableExists = false,
        userIds = null,
        companyIds = null,
        tableName = schemas.userCompaniesTable,
        tableSchema = schemas.userCompanies,
        userEntries = entries.users1.records,
        userKeys = entries.users1.keys,
        compEntries = entries.companies1.records,
        compKeys = entries.companies1.keys,
        tableReady = false,
        tableIds = null;

    

    /** 
     * Memorizza l'istante iniziale del test.
     * 
     * Viene utilizzata per il TIMING SE ABILITATO */ 
    let start;

    // ~~~~~~~~~~~~~~~~ PER OGNI TEST CASE UN BLOCCO `it()`~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
    // Anteporre `async` se la funzione da testare è asyncrona (cb o promise)                                       //
    it(`UPD.CR01 - Restituisce \`queryResult.success: true\` ed aggiorna il record con argomenti conformi (CROSS)`, async function(){

        let thisHeader = 'UPD.CR01'
        let header = `${mainHeader} - ${thisHeader}`;

        // Skip del test in base ad alcune condizioni
        if (!initDone) {
            /** Motivo dello skip */
            let reason = 'Fallita inizializzazione del driver';
            console.warn(`\t# TEST SKIPPED: ${reason} #`);
            this.skip();
        };

        if (!tableExists) {
            /** Motivo dello skip */
            let reason = 'Fallita creazione della table di test';
            console.warn(`\t# TEST SKIPPED: ${reason} #`);
            this.skip();
        };

        if(!userIds || !companyIds) {
            /** Motivo dello skip */
            let reason = `Fallito inserimento id per la table \`users\` (inserted: ${userIds.length}) o \`companies\` (inserted: ${companyIds.length}) `;
            console.warn(`\t# TEST SKIPPED: ${reason} #`);
            this.skip();
        };
        
        if (!tableReady) {
            /** Motivo dello skip */
            let reason = 'Fallito inserimento nella table cross-referenced';
            console.warn(`\t# TEST SKIPPED: ${reason} #`);
            this.skip();
        };



        let values = [ companyIds[0], userIds[6] ];
        if (DEBUG_ENABLED) console.debug(logStr(`tableIds: ${tableIds}`));
        /** Argomenti da utilizzare per la chiamata alla funzione */
        let arguments = [tableName, tableIds[0], entries.userComp1.keys, values];
        
        if (TIMEIT_ENABLED) start = hrtime();
        let res = await driver.update(...arguments);
        if (TIMEIT_ENABLED) timeRes.add(thisHeader, hrtime(start), arguments)
        
        if (DEBUG_ENABLED) console.debug(logStr(`${header} - Res: ${res.toString()}`));
        if (DEBUG_ENABLED) console.debug(logStr(`Query: ${res.query}`));
        if (DEBUG_ENABLED) console.debug(logStr(`Params: ${res.params}`));
        if (DEBUG_ENABLED) console.debug(logStr(`Message: ${res.message}`));
        
        expect(res.success).equal(true);
        expect(res.affected).equal(1);
        
        
    }); // ~~~~~~~~~~~~~ FINE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

    // ~~~~~~~~~~~~~~~~ PER OGNI TEST CASE UN BLOCCO `it()`~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
    // Anteporre `async` se la funzione da testare è asyncrona (cb o promise)                                       //
    // it(`UPD.CR02 - Restituisce \`queryResult.success: false\` e NON inserisce il record con con argomento 'keys' non valido (chiavi errate)`, async function(){

    //     let thisHeader = 'UPD.CR02'
    //     let header = `${mainHeader} - ${thisHeader}`;

    //     // Skip del test in base ad alcune condizioni
    //     if (!initDone) {
    //         /** Motivo dello skip */
    //         let reason = 'Fallita inizializzazione del driver';
    //         console.warn(`\t# TEST SKIPPED: ${reason} #`);
    //         this.skip();
    //     };

    //     if (!tableExists) {
    //         /** Motivo dello skip */
    //         let reason = 'Fallita creazione della table di test';
    //         console.warn(`\t# TEST SKIPPED: ${reason} #`);
    //         this.skip();
    //     };

    //     if(!userIds || !companyIds) {
    //         /** Motivo dello skip */
    //         let reason = `Fallito inserimento id per la table \`users\` (inserted: ${userIds.length}) o \`companies\` (inserted: ${companyIds.length}) `;
    //         console.warn(`\t# TEST SKIPPED: ${reason} #`);
    //         this.skip();
    //     };
        
    //     if (!tableReady) {
    //         /** Motivo dello skip */
    //         let reason = 'Fallito inserimento nella table cross-referenced';
    //         console.warn(`\t# TEST SKIPPED: ${reason} #`);
    //         this.skip();
    //     };



    //     let values = [ companyIds[0], userIds[0] ];
        
    //     // Cicla le chiavi del record per modificarne una alla volta
    //     for (let i=0; i<entries.userComp1.keys.length; i++) {
    //         let wrongKeys = jsutils.io.copyObj(entries.userComp1.keys);
    //         wrongKeys[i] = wrongKeys[i]+"_WRONG";
    //         /** Argomenti da utilizzare per la chiamata alla funzione */
    //         let arguments = [tableName, wrongKeys, values];
            
    //         if (TIMEIT_ENABLED) start = hrtime();
    //         let res = await driver.insert(...arguments);
    //         if (TIMEIT_ENABLED) timeRes.add(thisHeader, hrtime(start), arguments)
            
    //         if (DEBUG_ENABLED) console.debug(logStr(`${header} - Res: ${res}`));
            
    //         expect(res.success).equal(false);
            
    //     }
        
        
        
    // }); // ~~~~~~~~~~~~~ FINE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

    
    // ~~~~~~~~~~~~~~~~ PER OGNI TEST CASE UN BLOCCO `it()`~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
    // Anteporre `async` se la funzione da testare è asyncrona (cb o promise)                                       //
    // it(`UPD.CR03 - Restituisce \`queryResult.success: false\` e NON inserisce il record con con argomento 'values' non valido (referenze errate)`, async function(){

    //     let thisHeader = 'UPD.CR03'
    //     let header = `${mainHeader} - ${thisHeader}`;

    //     // Skip del test in base ad alcune condizioni
    //     if (!initDone) {
    //         /** Motivo dello skip */
    //         let reason = 'Fallita inizializzazione del driver';
    //         console.warn(`\t# TEST SKIPPED: ${reason} #`);
    //         this.skip();
    //     };

    //     if (!tableExists) {
    //         /** Motivo dello skip */
    //         let reason = 'Fallita creazione della table di test';
    //         console.warn(`\t# TEST SKIPPED: ${reason} #`);
    //         this.skip();
    //     };

    //     if(!userIds || !companyIds) {
    //         /** Motivo dello skip */
    //         let reason = `Fallito inserimento id per la table \`users\` (inserted: ${userIds.length}) o \`companies\` (inserted: ${companyIds.length}) `;
    //         console.warn(`\t# TEST SKIPPED: ${reason} #`);
    //         this.skip();
    //     };

    //     if (!tableReady) {
    //         /** Motivo dello skip */
    //         let reason = 'Fallito inserimento nella table cross-referenced';
    //         console.warn(`\t# TEST SKIPPED: ${reason} #`);
    //         this.skip();
    //     };


    //     let wrongValues = [ 
    //         [333, userIds[0]], 
    //         [companyIds[0], 666]
    //     ];
        
    //     // Cicla le chiavi del record per modificarne una alla volta
    //     for (let i=0; i<wrongValues.length; i++) {
    //         /** Argomenti da utilizzare per la chiamata alla funzione */
    //         let arguments = [tableName, entries.userComp1.keys, wrongValues[i]];
            
    //         if (TIMEIT_ENABLED) start = hrtime();
    //         let res = await driver.insert(...arguments);
    //         if (TIMEIT_ENABLED) timeRes.add(thisHeader, hrtime(start), arguments)
            
    //         if (DEBUG_ENABLED) console.debug(logStr(`${header} - Res: ${res}`));
            
    //         expect(res.success).equal(false);
            
    //     }
        
        
        
    // }); // ~~~~~~~~~~~~~ FINE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

    

// ============================================================================================ //
//                                IMPOSTAZIONI DEI TEST                                         //
// ============================================================================================ //

    /** Metodo eseguito **prima del'esecuzione di tutta la suite** @async */
    before( async function () {
        if (TIMEIT_ENABLED) {timeRes = new TimingResults(mainHeader);}

        // Creazione istanza del driver (condizione skip)
        driver = new tested.driverFactory('mysql', DEBUG_ENABLED, console, 0);
        
        // Copia a sè stante dei parametri con valore personalizzato per evitare interferenze con gli altri test
        let par = jsutils.io.copyObj(params.goodParams);
        par.connectionLimit = 2190
        ;

        // Inizializzazione del driver (istanza indipendente dagli altri test - v. sopra)
        initDone = await driver.init(par);

        // Creazione table referenziata (1/2) - USERS
        let resTb1 = await driver.ensureTable(schemas.userTable, schemas.userSch);

        // Table creata con successo
        if (resTb1.success) {
            // Array da popolare con id dei campi inseriti con successo
            let usrIds = [];
            // Cicla i record di test (USERS)
            for (let record of userEntries) {
                // Inserisce il record corrente
                let r = await driver.insert(schemas.userTable, userKeys, record);
                // Inserimento avvenuto con success: aggiunge l'id restituito all'array `usrIds`
                if (r.success) {usrIds.push(...r.insertedIds)};
            };
            // Se almeno un id è inserito, copia l'array locale in quello globale
            if (usrIds.length) {userIds = usrIds};
        };

        // Creazione table referenziata (2/2) - COMPANIES
        let resTb2 = await driver.ensureTable(schemas.companiesTable, schemas.companiesSch);

        // Table creata con successo
        if (resTb2.success) {
            // Array da popolare con id dei campi inseriti con successo
            let cmpIds = [];
            // Cicla i record di test (COMPANIES)
            for (let record of compEntries) {
                // Inserisce il record corrente
                let r = await driver.insert(schemas.companiesTable, compKeys, record);
                // Inserimento avvenuto con success: aggiunge l'id restituito all'array `usrIds`
                if (r.success) {cmpIds.push(...r.insertedIds)};
            };
            // Se almeno un id è inserito, copia l'array locale in quello globale
            if (cmpIds.length) {companyIds = cmpIds};
        };

        // Crea la table di test
        let tableExistsRes = await driver.ensureTable(tableName, tableSchema);
        // La tavola esiste in base a QueryResult.success 
        tableExists = tableExistsRes.success;

        if (tableExists) {
            let values = [ 
                [companyIds[0], userIds[0]],
                [companyIds[0], userIds[1]],
                [companyIds[0], userIds[2]],
                [companyIds[0], userIds[3]] 
            ];
            
            //
            let okIds = [];
            for (let value of values) {
                /** Argomenti da utilizzare per la chiamata alla funzione */
                let arguments = [tableName, entries.userComp1.keys, value];
                let res = await driver.insert(...arguments);
                if (res.success) { okIds.push(res.insertedIds)}
            };

            if (okIds.length) {
                tableIds = okIds;
                tableReady = true;
            }
        }
        
    });

    // /** Metodo eseguito **prima del'esecuzione di tutta la suite** */
    // before( function () {
    //     if (TIMEIT_ENABLED) {timeRes = new TimingResults(mainHeader);}
    // });

    /** Metodo eseguito **dopo l'esecuzione di tutta la suite** @async */
    after( async function () {
        if (TIMEIT_ENABLED) {console.info(timeRes.getResults())}
        
        // Elimina la table di test
        await driver._pool.query(`DROP TABLE ${tableName};`);
        // Eliminazione delle table referenziate nel test
        await driver._pool.query(`DROP TABLE ${schemas.userTable};`);
        await driver._pool.query(`DROP TABLE ${schemas.companiesTable};`);
        await driver.stop();
    });

    /** Metodo eseguito **dopo l'esecuzione di tutta la suite** */
    // after( function () {
    // });

    /** Metodo eseguito **prima del'esecuzione di ogni test nella suite** @async */
    // beforeEach( async function () {
    // });

    /** Metodo eseguito **prima del'esecuzione di  ogni test nella suite** */
    // beforeEach( function () {
    // });

    /** Metodo eseguito **dopo l'esecuzione di  ogni test nella suite** @async */
    afterEach( async function () {
        await entries.clearTable(driver, tableName);
    });

    /** Metodo eseguito **dopo l'esecuzione di  ogni test nella suite** */
    // afterEach( function () {
    // });

}); /** ---------------- fine della suite #1 ----------------------------------------*/