
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DEL MODULO DA TESTARE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
const tested = require("../");
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DEI MODULI NECESSARI ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
/** Utilità varie */
const jsutils           = require('leukos-tech-jsutils');

/** Funzione di test utilizzata in associazione a `mocha` */
const expect            = require('chai').expect;
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GESTIONE DEBUG ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
/** Proprietà di `process.env` in cui ricercare il valore indicante l'attivazione della modalità DEBUG (Verbose) dei test  */
const env_DEBUG_PROP    = process.env.DEBUG;

/** Se `true` i test vengono eseguiti loggando informazioni estese. Basata su `env_DEBUG_PROP`. */
const DEBUG_ENABLED     = (['1', 'true'].includes(env_DEBUG_PROP))

// console.warn(`DEBUG ENABLED: ${DEBUG_ENABLED}`)
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GESTIONE TIMING ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
/** Proprietà di `process.env` relativa al timing */
const env_TIMING_PROP   = process.env.TIMEIT;

/** Se `true' o 1 i l'esecuzione delle funzioni viene cronometrata */
const TIMEIT_ENABLED    = (['1', 1, 'true', true]).includes(env_TIMING_PROP);
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SHORT-ALIASES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

/**Formatta gli header per le intestazioni dei test (metodi `describe()`) */
const getHeader         = jsutils.tests.getHeader;
/** Formatta una stringa da loggare aggiungendo info come il nome della funzione */
const logStr            = jsutils.log.logString;
/** Handler risultati timing */
const TimingResults     = jsutils.tests.timeIt;
/** Short-Alias per `process.hrtime` */
const hrtime            = process.hrtime;
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DELLE RISORSE E MODULI ADDIZIONALI ~~~~~~~~~~~~~~~~~~~~~//
const params = require('./resources/poolParams');

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//


// ============================================================================================ //
//                                DEFINIZIONE DEI TEST                                          //
// ============================================================================================ //

/** Header principale */
const mainHeader = 'MysqlDriver.init(poolParams) #interface #initialization #mysql';

describe (`${getHeader(mainHeader, 0)}`, function() {


    this.timeout(0);
    
    // Variabili con Scope locale a questa unità vanno qui
    
    let driver=null;
    let connlim = 3333;
    let par;
    /** 
     * Memorizza l'istante iniziale del test.
     * 
     * Viene utilizzata per il TIMING SE ABILITATO */ 
    let start;

    // ~~~~~~~~~~~~~~~~ PER OGNI TEST CASE UN BLOCCO `it()`~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
    // Anteporre `async` se la funzione da testare è asyncrona (cb o promise)                                       //
    it(`INI01 - Si risolve in \'true\' quando vengono forniti parametri corretti `, async function(){

        let thisHeader = 'INI01'
        let header = `${mainHeader} - ${thisHeader}`;

        // Skip del test in base ad alcune condizioni
        if (!driver) {
            /** Motivo dello skip */
            let reason = 'Istanza del driver non ottenuta';
            console.warn(`\t# TEST SKIPPED: ${reason} #`);
            this.skip();
        } 
        
        /** Argomenti da utilizzare per la chiamata alla funzione */
        let arguments = [par];
        
        if (TIMEIT_ENABLED) start = hrtime();
        let res = await driver.init(...arguments);
        if (TIMEIT_ENABLED) timeRes.add(thisHeader, hrtime(start), arguments)
        
        if (DEBUG_ENABLED) console.debug(logStr(`${header} - Res: ${res}`));
        
        expect(res).equal(true);
        
    }); // ~~~~~~~~~~~~~ FINE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //


    // ~~~~~~~~~~~~~~~~ PER OGNI TEST CASE UN BLOCCO `it()`~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
    // Anteporre `async` se la funzione da testare è asyncrona (cb o promise)                                       //
    it(`INI02 - Solleva \'MissingArgError\' quando manca una delle proprietà necessarie ai parametri`, async function(){

        let thisHeader = 'INI02'
        let header = `${mainHeader} - ${thisHeader}`;

        // Skip del test in base ad alcune condizioni
        if (!driver) {
            /** Motivo dello skip */
            let reason = 'Istanza del driver non ottenuta';
            console.warn(`\t# TEST SKIPPED: ${reason} #`);
            this.skip();
        } 
        
        // Il test avviene per ogni proprietà mandatory
        for (let i=0; i<3; i++) {


            _par = jsutils.io.copyObj(par);

            switch(i){
                case 0:
                    delete _par.user;
                    break;
                case 1:
                    delete _par.password;
                    break;
                case 2:
                    delete _par.database;
            };

            /** Argomenti da utilizzare per la chiamata alla funzione */
            let arguments = [_par];
            
            try {
                if (TIMEIT_ENABLED) start = hrtime();
                // Solleverà MissingArgError
                let res = await driver.init(...arguments);
            
            } catch (err) {
                if (TIMEIT_ENABLED) timeRes.add(`${thisHeader} - Eccezione`, hrtime(start), arguments)
                expect(err.name).equal('MissingArgError');
            }
            
            
        }
        
        
    }); // ~~~~~~~~~~~~~ FINE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //


    // ~~~~~~~~~~~~~~~~ PER OGNI TEST CASE UN BLOCCO `it()`~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
    // Anteporre `async` se la funzione da testare è asyncrona (cb o promise)                                       //
    it(`INI03 - Solleva \'WrongArgTypeError\' quando manca una delle proprietà necessarie ai parametri`, async function(){

        let thisHeader = 'INI03'
        let header = `${mainHeader} - ${thisHeader}`;

        // Skip del test in base ad alcune condizioni
        if (!driver) {
            /** Motivo dello skip */
            let reason = 'Istanza del driver non ottenuta';
            console.warn(`\t# TEST SKIPPED: ${reason} #`);
            this.skip();
        } 
        
        // Il test avviene per ogni proprietà mandatory
        for (let i=0; i<3; i++) {


            _par = jsutils.io.copyObj(par);

            switch(i){
                case 0:
                    _par.user = 1;
                    break;
                case 1:
                    _par.password = 1;
                    break;
                case 2:
                    _par.database = 1;
            };

            /** Argomenti da utilizzare per la chiamata alla funzione */
            let arguments = [_par];
            
            try {
                if (TIMEIT_ENABLED) start = hrtime();
                // Solleverà MissingArgError
                let res = await driver.init(...arguments);
            
            } catch (err) {
                // console.log(err)
                if (TIMEIT_ENABLED) timeRes.add(`${thisHeader} - Eccezione`, hrtime(start), arguments)
                expect(err.name).equal('WrongArgTypeError');
            }
            
            
        }
        
        
    }); // ~~~~~~~~~~~~~ FINE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

    // ~~~~~~~~~~~~~~~~ PER OGNI TEST CASE UN BLOCCO `it()`~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
    // Anteporre `async` se la funzione da testare è asyncrona (cb o promise)                                       //
    it(`INI04 - Si risolve in \'false\' se unsername o password per la connessione vengono forniti errati `, async function(){

        let thisHeader = 'INI04'
        let header = `${mainHeader} - ${thisHeader}`;

        // Skip del test in base ad alcune condizioni
        if (!driver) {
            /** Motivo dello skip */
            let reason = 'Istanza del driver non ottenuta';
            console.warn(`\t# TEST SKIPPED: ${reason} #`);
            this.skip();
        } 
        
        // Il test avviene per ogni proprietà mandatory
        for (let i=0; i<2; i++) {


            _par = jsutils.io.copyObj(par);

            switch(i){
                case 0:
                    _par.user = "aMissingUser";
                    break;
                case 1:
                    _par.password = "aVeryWrongPassword";
                    break;
                
            };

            /** Argomenti da utilizzare per la chiamata alla funzione */
            let arguments = [_par];
            
            if (TIMEIT_ENABLED) start = hrtime();
            // Solleverà MissingArgError
            let res = await driver.init(...arguments);
                
            if (TIMEIT_ENABLED) timeRes.add(`${thisHeader}`, hrtime(start), arguments)
            expect(res).equal(false);
            
            
            
        }
        
        
    }); // ~~~~~~~~~~~~~ FINE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //


    // ~~~~~~~~~~~~~~~~ PER OGNI TEST CASE UN BLOCCO `it()`~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
    // Anteporre `async` se la funzione da testare è asyncrona (cb o promise)                                       //
    it(`INI05 - Si risolve in \'true\' e restituisce Singleton del driver tra loro correlati se inizializzato con oggetti indipendenti contenenti gli stessi valori`, async function(){

        let thisHeader = 'INI05'
        let header = `${mainHeader} - ${thisHeader}`;

        // Skip del test se l'istanza del driver non viene ottenuta
        if (!driver) {
            /** Motivo dello skip */
            let reason = 'Istanza del driver non ottenuta';
            console.warn(`\t# TEST SKIPPED: ${reason} #`);
            this.skip();
        } 
        
        let parCopy = jsutils.io.copyObj(par);

        // Se gli oggetti risultano uguali il test viene saltato e la cosa segnalata
        if (par==parCopy) {
            let reason = 'Gli oggetti parametri per il test risultano uguali';
            console.warn(`\t# TEST SKIPPED: ${reason} #`);
            this.skip();
        }
        /** Argomenti da utilizzare per la chiamata alla funzione */
        let arguments = [par];
        
        // Inizializzazione oggetto 1 (con timing)
        if (TIMEIT_ENABLED) start = hrtime();
        let res = await driver.init(...arguments);
        if (TIMEIT_ENABLED) timeRes.add(thisHeader, hrtime(start), arguments)
        
        if (DEBUG_ENABLED) console.debug(logStr(`${header} - Res: ${res}`));
        
        expect(res).equal(true);

        // Aggiunge una nuova proprietà all'oggetto pool
        driver._pool.newProp = "HELLO";

        // Inizializzazione oggetto 2
        let driver2 = new tested.driverFactory('mysql', DEBUG_ENABLED, console);
        let res2 = await driver2.init(parCopy);

        expect(res2).equal(true);
        expect(driver._pool.poolName).equal(driver2._pool.poolName);
        
        // La proprietà aggiunta all'altra pool deve essere ritrovata anche in quasta istanza
        expect(driver2._pool.newProp).equal("HELLO");

        
    }); // ~~~~~~~~~~~~~ FINE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

    // ~~~~~~~~~~~~~~~~ PER OGNI TEST CASE UN BLOCCO `it()`~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
    // Anteporre `async` se la funzione da testare è asyncrona (cb o promise)                                       //
    it(`INI06 - Si risolve in \'false\' se fornito un valore errato per le proprietà 'host' o 'port'`, async function(){

        let thisHeader = 'INI06'
        let header = `${mainHeader} - ${thisHeader}`;

        // Skip del test in base ad alcune condizioni
        if (!driver) {
            /** Motivo dello skip */
            let reason = 'Istanza del driver non ottenuta';
            console.warn(`\t# TEST SKIPPED: ${reason} #`);
            this.skip();
        } 
        
        _par = jsutils.io.copyObj(par);

        _par.host = 'www.unexistinghost.com'

        /** Argomenti da utilizzare per la chiamata alla funzione */
        let arguments = [_par];
        
        if (TIMEIT_ENABLED) start = hrtime();
        
        let res = await driver.init(...arguments);
            
        if (TIMEIT_ENABLED) timeRes.add(`${thisHeader}`, hrtime(start), arguments)
        expect(res).equal(false);

        _par2 = jsutils.io.copyObj(par);

        
        _par2.host = 'localhost';
        _par2.port = 1111;

        /** Argomenti da utilizzare per la chiamata alla funzione */
        arguments = [_par2];
        
        if (TIMEIT_ENABLED) start = hrtime();
        
        res = await driver.init(...arguments);
            
        if (TIMEIT_ENABLED) timeRes.add(`${thisHeader}`, hrtime(start), arguments)
        expect(res).equal(false);
        
        
    }); // ~~~~~~~~~~~~~ FINE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //


// ============================================================================================ //
//                                IMPOSTAZIONI DEI TEST                                         //
// ============================================================================================ //

    /** Metodo eseguito **prima del'esecuzione di tutta la suite** @async */
    before( async function () {
        if (TIMEIT_ENABLED) {timeRes = new TimingResults(mainHeader);}
    });

    // /** Metodo eseguito **prima del'esecuzione di tutta la suite** */
    // before( function () {
    //     if (TIMEIT_ENABLED) {timeRes = new TimingResults(mainHeader);}
    // });

    /** Metodo eseguito **dopo l'esecuzione di tutta la suite** @async */
    after( async function () {
        if (TIMEIT_ENABLED) {console.info(timeRes.getResults())}
    });

    /** Metodo eseguito **dopo l'esecuzione di tutta la suite** */
    // after( function () {
    // });

    /** Metodo eseguito **prima del'esecuzione di ogni test nella suite** @async */
    beforeEach( async function () {
        // Crea una copia dei parametri per ogni test
        par = jsutils.io.copyObj(params.goodParams);
        par.connectionLimit = connlim;
        driver = new tested.driverFactory('mysql', DEBUG_ENABLED, console, 0);
        connlim++;
    });

    /** Metodo eseguito **prima del'esecuzione di  ogni test nella suite** */
    // beforeEach( function () {
    // });

    /** Metodo eseguito **dopo l'esecuzione di  ogni test nella suite** @async */
    afterEach( async function () {
        await driver.stop(true);
        driver=null;
    });

    /** Metodo eseguito **dopo l'esecuzione di  ogni test nella suite** */
    // afterEach( function () {
    // });

}); /** ---------------- fine della suite #1 ----------------------------------------*/