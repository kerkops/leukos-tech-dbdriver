
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DEL MODULO DA TESTARE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
const tested = require("../")
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DEI MODULI NECESSARI ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
/** Utilità varie */
const jsutils           = require('leukos-tech-jsutils');

/** Funzione di test utilizzata in associazione a `mocha` */
const expect            = require('chai').expect;
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GESTIONE DEBUG ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
/** Proprietà di `process.env` in cui ricercare il valore indicante l'attivazione della modalità DEBUG (Verbose) dei test  */
const env_DEBUG_PROP    = process.env.DEBUG;

/** Se `true` i test vengono eseguiti loggando informazioni estese. Basata su `env_DEBUG_PROP`. */
const DEBUG_ENABLED     = (['1', 'true'].includes(env_DEBUG_PROP))

console.warn(`DEBUG ENABLED: ${DEBUG_ENABLED}`)
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GESTIONE TIMING ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
/** Proprietà di `process.env` relativa al timing */
const env_TIMING_PROP   = process.env.TIMEIT;

/** Se `true' o 1 i l'esecuzione delle funzioni viene cronometrata */
const TIMEIT_ENABLED    = (['1', 1, 'true', true]).includes(env_TIMING_PROP);
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SHORT-ALIASES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

/**Formatta gli header per le intestazioni dei test (metodi `describe()`) */
const getHeader         = jsutils.tests.getHeader;
/** Formatta una stringa da loggare aggiungendo info come il nome della funzione */
const logStr            = jsutils.log.logString;
/** Handler risultati timing */
const TimingResults     = jsutils.tests.timeIt;
/** Short-Alias per `process.hrtime` */
const hrtime            = process.hrtime;
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DELLE RISORSE E MODULI ADDIZIONALI ~~~~~~~~~~~~~~~~~~~~~//
const params = require('./resources/poolParams');
const schemas = require('./resources/dbSchemas');
const entries = require('./resources/dbEntries');

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//


// ============================================================================================ //
//                                DEFINIZIONE DEI TEST                                          //
// ============================================================================================ //

/** Header principale */
const mainHeader = 'MysqlDriver.delete(tableName, ids) #interface #data-crud #data-delete #mysql';

describe (`${getHeader(mainHeader, 0)}`, function() {

    this.timeout(0);
    // Variabili con Scope locale a questa unità vanno qui
    let initDone = false;
    let driver;
    let insertedIds = null;
    let tableExists = false;
    let testTableName = "MyTable";
    /** 
     * Memorizza l'istante iniziale del test.
     * 
     * Viene utilizzata per il TIMING SE ABILITATO */ 
    let start;

    // ~~~~~~~~~~~~~~~~ PER OGNI TEST CASE UN BLOCCO `it()`~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
    // Anteporre `async` se la funzione da testare è asyncrona (cb o promise)                                       //
    it(`DEL01 - Risolve in \`queryResult.success: true\` ed elimina gli id indicati se corretti `, async function(){

        let thisHeader = 'DEL01'
        let header = `${mainHeader} - ${thisHeader}`;
        
        // Skip del test in base ad alcune condizioni
        if (!initDone) {
            /** Motivo dello skip */
            let reason = 'Inizializzazione del driver FALLITA';
            console.warn(`\t# TEST SKIPPED: ${reason} #`);
            this.skip();
        };

        if (!insertedIds) {
            /** Motivo dello skip */
            let reason = 'FALLITO inserimento record a scopo di test';
            console.warn(`\t# TEST SKIPPED: ${reason} #`);
            this.skip();
        };



        
        
        let last = insertedIds.pop();

        /** Argomenti da utilizzare per la chiamata alla funzione */
        let arguments = [testTableName, insertedIds];
        
        
        if (TIMEIT_ENABLED) start = hrtime();
        let res = await driver.delete(...arguments);
        if (TIMEIT_ENABLED) timeRes.add(thisHeader, hrtime(start), arguments)
        
        if (DEBUG_ENABLED) console.debug(logStr(`${header} - Res: ${res}`));
        
        expect(res.success).equal(true);
        expect(res.affected).equal(insertedIds.length);
        
    }); // ~~~~~~~~~~~~~ FINE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

    // ~~~~~~~~~~~~~~~~ PER OGNI TEST CASE UN BLOCCO `it()`~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
    // Anteporre `async` se la funzione da testare è asyncrona (cb o promise)                                       //
    it(`DEL02 - Risolve in \`queryResult.success: true\` ed elimina gli id corretti anche se presenti id errati nell'array fornito `, async function(){

        let thisHeader = 'DEL02'
        let header = `${mainHeader} - ${thisHeader}`;
        
        // Skip del test in base ad alcune condizioni
        if (!initDone) {
            /** Motivo dello skip */
            let reason = 'Inizializzazione del driver FALLITA';
            console.warn(`\t# TEST SKIPPED: ${reason} #`);
            this.skip();
        };

        if (!insertedIds) {
            /** Motivo dello skip */
            let reason = 'FALLITO inserimento record a scopo di test';
            console.warn(`\t# TEST SKIPPED: ${reason} #`);
            this.skip();
        };


        // Numero di id da eliminare (presenti nel db)
        let toBeDeleted = insertedIds.length;
        // Aggiunta di id errati
        insertedIds.push(159, 777, 5678);

        /** Argomenti da utilizzare per la chiamata alla funzione */
        let arguments = [testTableName, insertedIds];
        
        
        if (TIMEIT_ENABLED) start = hrtime();
        let res = await driver.delete(...arguments);
        if (TIMEIT_ENABLED) timeRes.add(thisHeader, hrtime(start), arguments)
        
        if (DEBUG_ENABLED) console.debug(logStr(`${header} - Res: ${res}`));
        
        expect(res.success).equal(true);
        expect(res.affected).equal(toBeDeleted);
        
    }); // ~~~~~~~~~~~~~ FINE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

    // ~~~~~~~~~~~~~~~~ PER OGNI TEST CASE UN BLOCCO `it()`~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
    // Anteporre `async` se la funzione da testare è asyncrona (cb o promise)                                       //
    it(`DEL03 - Risolve in \`queryResult.success: false\` e non elimina gli id forniti se errati`, async function(){

        let thisHeader = 'DEL03'
        let header = `${mainHeader} - ${thisHeader}`;
        
        // Skip del test in base ad alcune condizioni
        if (!initDone) {
            /** Motivo dello skip */
            let reason = 'Inizializzazione del driver FALLITA';
            console.warn(`\t# TEST SKIPPED: ${reason} #`);
            this.skip();
        };

        
        /** Argomenti da utilizzare per la chiamata alla funzione */
        let arguments = [testTableName, [8000, 9000, 10000, 11000]];
        
        
        if (TIMEIT_ENABLED) start = hrtime();
        let res = await driver.delete(...arguments);
        if (TIMEIT_ENABLED) timeRes.add(thisHeader, hrtime(start), arguments)
        
        if (DEBUG_ENABLED) console.debug(logStr(`${header} - Res: ${res}`));
        
        expect(res.success).equal(false);
        expect(res.affected).equal(0);
        
    }); // ~~~~~~~~~~~~~ FINE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

// ============================================================================================ //
//                                IMPOSTAZIONI DEI TEST                                         //
// ============================================================================================ //

    /** Metodo eseguito **prima del'esecuzione di tutta la suite** @async */
    before( async function () {
        let pars = jsutils.io.copyObj(params.goodParams);
        // Rende il Singleton indipendente dagli altri testati
        pars.connectionLimit = 17;

        // Genera l'istanza da testare
        driver = new tested.driverFactory('mysql', DEBUG_ENABLED, console, 0);

        // nizializza l'istanza
        initDone = await driver.init(pars);

        // Assicura l'esistenza della table di test
        tableExists = await driver.ensureTable(testTableName, schemas.userSch);

        if (TIMEIT_ENABLED) {timeRes = new TimingResults(mainHeader);}
    });

    /** Metodo eseguito **dopo l'esecuzione di tutta la suite** @async */
    after( async function () {
        if (TIMEIT_ENABLED) {console.info(timeRes.getResults())}

        await driver._pool.query(`DROP TABLE ${testTableName};`);
        // Arresta la pool del driver
        await driver.stop();
    });

    /** Metodo eseguito **prima del'esecuzione di ogni test nella suite** @async */
    beforeEach( async function () {
        // await entries.clearTable(driver, testTableName);
        insertedIds = await entries.populateTable(driver, testTableName, entries.users1);

    });

    /** Metodo eseguito **prima del'esecuzione di  ogni test nella suite** */
    // beforeEach( function () {
    // });

    /** Metodo eseguito **dopo l'esecuzione di  ogni test nella suite** @async */
    afterEach( async function () {
        insertedIds = null;
        await entries.clearTable(driver, testTableName);
    });

    /** Metodo eseguito **dopo l'esecuzione di  ogni test nella suite** */
    // afterEach( function () {
    // });

}); /** ---------------- fine della suite #1 ----------------------------------------*/