
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DEL MODULO DA TESTARE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
const tested = require("../")
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DEI MODULI NECESSARI ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
/** Utilità varie */
const jsutils           = require('leukos-tech-jsutils');

/** Funzione di test utilizzata in associazione a `mocha` */
const expect            = require('chai').expect;
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GESTIONE DEBUG ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
/** Proprietà di `process.env` in cui ricercare il valore indicante l'attivazione della modalità DEBUG (Verbose) dei test  */
const env_DEBUG_PROP    = process.env.DEBUG;

/** Se `true` i test vengono eseguiti loggando informazioni estese. Basata su `env_DEBUG_PROP`. */
const DEBUG_ENABLED     = (['1', 'true'].includes(env_DEBUG_PROP))

// console.warn(`DEBUG ENABLED: ${DEBUG_ENABLED}`)
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GESTIONE TIMING ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
/** Proprietà di `process.env` relativa al timing */
const env_TIMING_PROP   = process.env.TIMEIT;

/** Se `true' o 1 i l'esecuzione delle funzioni viene cronometrata */
const TIMEIT_ENABLED    = (['1', 1, 'true', true]).includes(env_TIMING_PROP);
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SHORT-ALIASES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

/**Formatta gli header per le intestazioni dei test (metodi `describe()`) */
const getHeader         = jsutils.tests.getHeader;
/** Formatta una stringa da loggare aggiungendo info come il nome della funzione */
const logStr            = jsutils.log.logString;
/** Handler risultati timing */
const TimingResults     = jsutils.tests.timeIt;
/** Short-Alias per `process.hrtime` */
const hrtime            = process.hrtime;
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DELLE RISORSE E MODULI ADDIZIONALI ~~~~~~~~~~~~~~~~~~~~~//
const params = require('./resources/poolParams');
const schemas = require('./resources/dbSchemas');
const entries = require('./resources/dbEntries');

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//


// ============================================================================================ //
//                                DEFINIZIONE DEI TEST                                          //
// ============================================================================================ //

/** Header principale */
const mainHeader = 'MysqlDriver.update(tableName, ids, keys, values) #interface #data-update #data-crud #mysql';

describe (`${getHeader(mainHeader, 0)}`, function() {
    
    this.timeout(0);

    // Variabili con Scope locale a questa unità vanno qui
    let initDone = false;
    let driver;
    let insertedIds = null;
    let tableExists = false;
    let testTableName = "MyTable";
    
    /** Memorizza l'istante iniziale del test (SE TIMING ATTIVO). */ 
    let start;
    
    // Altre variabili con Scope locale a questa unità vanno qui
    
    

    // ~~~~~~~~~~~~~~~~~~~~~~~ INIZIO DEFINIZIONE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
    it(`UPD01 - Risolve in \'queryResult.success: true\' ed aggiorna il record specificato con parametri conformi `, async function(){

        let thisHeader = 'UPD01'
        let header = `${mainHeader} - ${thisHeader}`;

        // Skip del test in base ad alcune condizioni
        if (!initDone) {
            /** Motivo dello skip */
            let reason = 'Inizializzazione del driver FALLITA';
            console.warn(`\t# TEST SKIPPED: ${reason} #`);
            this.skip();
        };

        if (!insertedIds) {
            /** Motivo dello skip */
            let reason = 'FALLITO inserimento record a scopo di test';
            console.warn(`\t# TEST SKIPPED: ${reason} #`);
            this.skip();
        };
        
        let updValues = jsutils.io.copyObj(entries.users1.records[1]);

        updValues[0] = "A_brand_new_name";
        updValues[1] = "A_brand_new_email";

        /** Argomenti da utilizzare per la chiamata alla funzione */
        let arguments = [testTableName, [insertedIds[1]], entries.users1.keys, updValues];
        
        if (TIMEIT_ENABLED) start = hrtime();
        let res = await driver.update(...arguments);
        if (TIMEIT_ENABLED) timeRes.add(thisHeader, hrtime(start), arguments)
        
        if (DEBUG_ENABLED) console.debug(logStr(`${header} - Res: ${res}`));
        
        expect(res.success).equal(true);
        
    }); // ~~~~~~~~~~~~~~~~~~~ FINE DEFINIZIONE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //



// ============================================================================================ //
//                                IMPOSTAZIONI DEI TEST                                         //
//                   (rimuovere `async` per testase FUNZIONI SINCRONE)                          //
// ============================================================================================ //

    /** Metodo eseguito **prima del'esecuzione di tutta la suite** @async */
    before( async function () {
        let pars = jsutils.io.copyObj(params.goodParams);
        // Rende il Singleton indipendente dagli altri testati
        pars.connectionLimit = 1717;

        // Genera l'istanza da testare
        driver = new tested.driverFactory('mysql', DEBUG_ENABLED, console, 1);

        // nizializza l'istanza
        initDone = await driver.init(pars);

        // Assicura l'esistenza della table di test
        tableExists = await driver.ensureTable(testTableName, schemas.userSch);

        if (TIMEIT_ENABLED) {timeRes = new TimingResults(mainHeader);}
    });

    /** Metodo eseguito **dopo l'esecuzione di tutta la suite** @async */
    after( async function () {
        if (TIMEIT_ENABLED) {console.info(timeRes.getResults())}
        await driver.stop();
    });

    /** Metodo eseguito **prima del'esecuzione di ogni test nella suite** @async */
    beforeEach( async function () {
        // Inserisce i record di test
        insertedIds = await entries.populateTable(driver, testTableName, entries.users1);
    });

    /** Metodo eseguito **dopo l'esecuzione di  ogni test nella suite** @async */
    afterEach( async function () {
        // Azzera id inseriti per prepararsi al prossimo test
        insertedIds = null;
        // svuota la table per prepararsi al prossimo test
        await entries.clearTable(driver, testTableName);
    });


}); /** ---------------- fine della suite #1 ----------------------------------------*/