
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DEL MODULO DA TESTARE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
const tested = require("../");
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DEI MODULI NECESSARI ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
/** Utilità varie */
const jsutils           = require('leukos-tech-jsutils');

/** Funzione di test utilizzata in associazione a `mocha` */
const expect            = require('chai').expect;
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GESTIONE DEBUG ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
/** Proprietà di `process.env` in cui ricercare il valore indicante l'attivazione della modalità DEBUG (Verbose) dei test  */
const env_DEBUG_PROP    = process.env.DEBUG;

/** Se `true` i test vengono eseguiti loggando informazioni estese. Basata su `env_DEBUG_PROP`. */
const DEBUG_ENABLED     = (['1', 'true'].includes(env_DEBUG_PROP))

// console.warn(`DEBUG ENABLED: ${DEBUG_ENABLED}`)
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GESTIONE TIMING ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
/** Proprietà di `process.env` relativa al timing */
const env_TIMING_PROP   = process.env.TIMEIT;

/** Se `true' o 1 i l'esecuzione delle funzioni viene cronometrata */
const TIMEIT_ENABLED    = (['1', 1, 'true', true]).includes(env_TIMING_PROP);
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SHORT-ALIASES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

/**Formatta gli header per le intestazioni dei test (metodi `describe()`) */
const getHeader         = jsutils.tests.getHeader;
/** Formatta una stringa da loggare aggiungendo info come il nome della funzione */
const logStr            = jsutils.log.logString;
/** Handler risultati timing */
const TimingResults     = jsutils.tests.timeIt;
/** Short-Alias per `process.hrtime` */
const hrtime            = process.hrtime;
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUIRE DELLE RISORSE E MODULI ADDIZIONALI ~~~~~~~~~~~~~~~~~~~~~//
const params = require('./resources/poolParams');
const schemas = require('./resources/dbSchemas');
const entries = require('./resources/dbEntries');
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//


// ============================================================================================ //
//                                DEFINIZIONE DEI TEST                                          //
// ============================================================================================ //

/** Header principale */
const mainHeader = 'MsqlDriver.insert(tableName, keys, values) #interface #data-crud #data-insert #mysql';

describe (`${getHeader(mainHeader, 0)}`, function() {
    this.timeout(0);
    // Variabili con Scope locale a questa unità vanno qui
    let tableName = "InsertTestTable";
    let driver;
    let initDone = false;
    let tableExists = false;

    /** 
     * Memorizza l'istante iniziale del test.
     * 
     * Viene utilizzata per il TIMING SE ABILITATO */ 
    let start;

    // ~~~~~~~~~~~~~~~~ PER OGNI TEST CASE UN BLOCCO `it()`~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
    // Anteporre `async` se la funzione da testare è asyncrona (cb o promise)                                       //
    it(`INS01 - Risolve in \`queryResult.success: true\` ed inserisce il record con argomenti conformi (campo AUTO_INCREMENT non fornito)`, async function(){

        let thisHeader = 'INS01'
        let header = `${mainHeader} - ${thisHeader}`;

        // Skip del test in base ad alcune condizioni
        if (!initDone) {
            /** Motivo dello skip */
            let reason = 'Fallita inizializzazione del driver';
            console.warn(`\t# TEST SKIPPED: ${reason} #`);
            this.skip();
        };

        if (!tableExists) {
            /** Motivo dello skip */
            let reason = 'Fallita creazione della table di test';
            console.warn(`\t# TEST SKIPPED: ${reason} #`);
            this.skip();
        };

        
        
        /** Argomenti da utilizzare per la chiamata alla funzione */
        let arguments = [tableName, entries.users1.keys, entries.users1.records[0]];
        
        if (TIMEIT_ENABLED) start = hrtime();
        let res = await driver.insert(...arguments);
        if (TIMEIT_ENABLED) timeRes.add(thisHeader, hrtime(start), arguments)
        
        if (DEBUG_ENABLED) console.debug(logStr(`${header} - Res: ${res}`));
        
        expect(res.success).equal(true);
        expect(res.affected).equal(1);
        expect(res.insertedIds.length).equal(1);
        
    }); // ~~~~~~~~~~~~~ FINE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

    // ~~~~~~~~~~~~~~~~ PER OGNI TEST CASE UN BLOCCO `it()`~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
    // Anteporre `async` se la funzione da testare è asyncrona (cb o promise)                                       //
    it(`INS02 - Risolve in \`queryResult.success: false\` e NON inserisce il record con con argomento 'keys' non valido (chiavi errate)`, async function(){

        let thisHeader = 'INS02'
        let header = `${mainHeader} - ${thisHeader}`;

        // Skip del test in base ad alcune condizioni
        if (!initDone) {
            /** Motivo dello skip */
            let reason = 'Fallita inizializzazione del driver';
            console.warn(`\t# TEST SKIPPED: ${reason} #`);
            this.skip();
        };

        if (!tableExists) {
            /** Motivo dello skip */
            let reason = 'Fallita creazione della table di test';
            console.warn(`\t# TEST SKIPPED: ${reason} #`);
            this.skip();
        };
        
        // Cicla le chiavi del record per modificarne una alla volta
        for (let i=0; i<entries.users1.keys.length; i++) {
            let wrongKeys = jsutils.io.copyObj(entries.users1.keys);
            wrongKeys[i] = wrongKeys[i]+"_WRONG";
            /** Argomenti da utilizzare per la chiamata alla funzione */
            let arguments = [tableName, wrongKeys, entries.users1.records[0]];
            
            if (TIMEIT_ENABLED) start = hrtime();
            let res = await driver.insert(...arguments);
            if (TIMEIT_ENABLED) timeRes.add(thisHeader, hrtime(start), arguments)
            
            if (DEBUG_ENABLED) console.debug(logStr(`${header} - Res: ${res}`));
            
            expect(res.success).equal(false);
            
        }
        
        
        
    }); // ~~~~~~~~~~~~~ FINE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

    // ~~~~~~~~~~~~~~~~ PER OGNI TEST CASE UN BLOCCO `it()`~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
    // Anteporre `async` se la funzione da testare è asyncrona (cb o promise)                                       //
    it(`INS03 - Risolve in \`queryResult.success: true\` ed inserisce il record con argomenti conformi (campo id - AUTO_INCREMENT - fornito)`, async function(){

        let thisHeader = 'INS03'
        let header = `${mainHeader} - ${thisHeader}`;

        // Skip del test in base ad alcune condizioni
        if (!initDone) {
            /** Motivo dello skip */
            let reason = 'Fallita inizializzazione del driver';
            console.warn(`\t# TEST SKIPPED: ${reason} #`);
            this.skip();
        };

        if (!tableExists) {
            /** Motivo dello skip */
            let reason = 'Fallita creazione della table di test';
            console.warn(`\t# TEST SKIPPED: ${reason} #`);
            this.skip();
        };

        
        let keys = jsutils.io.copyObj(entries.users1.keys);
        let records = jsutils.io.copyObj( entries.users1.records[0]);
        keys.unshift("id");
        records.unshift(111);

        /** Argomenti da utilizzare per la chiamata alla funzione */
        let arguments = [tableName, keys, records];
        
        if (TIMEIT_ENABLED) start = hrtime();
        let res = await driver.insert(...arguments);
        if (TIMEIT_ENABLED) timeRes.add(thisHeader, hrtime(start), arguments)
        
        if (DEBUG_ENABLED) console.debug(logStr(`${header} - Res: ${res}`));
        
        expect(res.success).equal(true);
        expect(res.affected).equal(1);
        expect(res.insertedIds.length).equal(1);
        
    }); // ~~~~~~~~~~~~~ FINE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

    // ~~~~~~~~~~~~~~~~ PER OGNI TEST CASE UN BLOCCO `it()`~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
    // Anteporre `async` se la funzione da testare è asyncrona (cb o promise)                                       //
    it(`INS04 - Risolve in \`queryResult.success: false\` e NON inserisce il record con con argomento 'values' non valido (numero di elementi errato)`, async function(){

        let thisHeader = 'INS02'
        let header = `${mainHeader} - ${thisHeader}`;

        // Skip del test in base ad alcune condizioni
        if (!initDone) {
            /** Motivo dello skip */
            let reason = 'Fallita inizializzazione del driver';
            console.warn(`\t# TEST SKIPPED: ${reason} #`);
            this.skip();
        };

        if (!tableExists) {
            /** Motivo dello skip */
            let reason = 'Fallita creazione della table di test';
            console.warn(`\t# TEST SKIPPED: ${reason} #`);
            this.skip();
        };
        
        // Cicla le chiavi del record per modificarne una alla volta
        for (let i=0; i<2; i++) {
            let wrongValues = jsutils.io.copyObj(entries.users1.records[1]);

            switch(i) {
                case 0:
                    wrongValues.push(1);
                    wrongValues.push("ciao");
                    wrongValues.push(true)
                    wrongValues.push("aloha");
                    // console.warn(`Wrong values (${wrongValues.length}): ${wrongValues}`)
                    break
                case 1:
                    wrongValues.pop();
                    wrongValues.pop();
                    // console.warn(`Wrong values (${wrongValues.length}): ${wrongValues}`)
                    break
                
            }

            
            /** Argomenti da utilizzare per la chiamata alla funzione */
            let arguments = [tableName, entries.users1.keys, wrongValues];
            
            if (TIMEIT_ENABLED) start = hrtime();
            let res = await driver.insert(...arguments);
            if (TIMEIT_ENABLED) timeRes.add(thisHeader, hrtime(start), arguments)
            
            if (DEBUG_ENABLED) console.debug(logStr(`${header} - Res: ${res}`));
            
            expect(res.success).equal(false);
            
        }
        
        
        
    }); // ~~~~~~~~~~~~~ FINE TEST CASE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

    

// ============================================================================================ //
//                                IMPOSTAZIONI DEI TEST                                         //
// ============================================================================================ //

    /** Metodo eseguito **prima del'esecuzione di tutta la suite** @async */
    before( async function () {
        if (TIMEIT_ENABLED) {timeRes = new TimingResults(mainHeader);}
        driver = new tested.driverFactory('mysql', DEBUG_ENABLED, console);
        
        // Copia a sè stante dei parametri con valore personalizzato per evitare interferenze con gli altri test
        let par = jsutils.io.copyObj(params.goodParams);
        par.connectionLimit = 217;

        initDone = await driver.init(par);

        // Crea la table di test
        let tableExistsRes = await driver.ensureTable(tableName, schemas.userSch);
        // La tavola esiste in base a QueryResult.success 
        tableExists = tableExistsRes.success;

    });

    // /** Metodo eseguito **prima del'esecuzione di tutta la suite** */
    // before( function () {
    //     if (TIMEIT_ENABLED) {timeRes = new TimingResults(mainHeader);}
    // });

    /** Metodo eseguito **dopo l'esecuzione di tutta la suite** @async */
    after( async function () {
        if (TIMEIT_ENABLED) {console.info(timeRes.getResults())}
        // Elimina la table di test
        await driver._pool.query(`DROP TABLE ${tableName};`);
        await driver.stop();
    });

    /** Metodo eseguito **dopo l'esecuzione di tutta la suite** */
    // after( function () {
    // });

    /** Metodo eseguito **prima del'esecuzione di ogni test nella suite** @async */
    // beforeEach( async function () {
    // });

    /** Metodo eseguito **prima del'esecuzione di  ogni test nella suite** */
    // beforeEach( function () {
    // });

    /** Metodo eseguito **dopo l'esecuzione di  ogni test nella suite** @async */
    afterEach( async function () {
        await entries.clearTable(driver, tableName);
    });

    /** Metodo eseguito **dopo l'esecuzione di  ogni test nella suite** */
    // afterEach( function () {
    // });

}); /** ---------------- fine della suite #1 ----------------------------------------*/